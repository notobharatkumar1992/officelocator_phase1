package com.officelocator.async;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.officelocator.AppDelegate;
import com.officelocator.Constants.Tags;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class LocationAddress {
    // public static final String OPEN_MAP_KEY =
    // "Fmjtd|luu82q6rnu%2Cb2%3Do5-94tnl6";
    public static final String OPEN_MAP_KEY = "Fmjtd|luu82q6rnu,b2=o5-94tnl6";

    public static String placeName = "", placeAdd = "", result = "", strAddress = "",
            geocodeQuality = "", street = "", adminArea1 = "",
            adminArea3 = "", adminArea4 = "", adminArea5 = "", city = "", state = "",
            country = "", postalCode = "";
    public static double latitude = 0.0, longitude = 0.0;
    private static String loc = "";

    public static void setNullValue() {
        placeName = "";
        placeAdd = "";
        result = "";
        geocodeQuality = "";
        street = "";
        adminArea3 = "";
        adminArea4 = "";
        adminArea5 = "";
        latitude = 0.0;
        longitude = 0.0;
    }

    public static void getAddressFromLocation(final double latitude,
                                              final double longitude, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                setNullValue();
                execute_Geocoder(latitude, longitude, context, handler);
            }
        };
        thread.start();
    }

    public static void execute_Geocoder(final double latitude,
                                        final double longitude, final Context context, final Handler handler) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocation(latitude,
                    longitude, 1);
            AppDelegate.LogT("addressList latitude = " + latitude
                    + ", longitude = " + longitude + ", addressList size = "
                    + addressList.size());
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                AppDelegate.LogUR("addressList = " + address);
                StringBuilder sb = new StringBuilder();
                placeName = address.getAddressLine(0);
                if (address.getAddressLine(1) != null && !address.getAddressLine(1).equalsIgnoreCase("null") && address.getAddressLine(0).length() > 0) {
                    placeName = placeName + ", " + address.getAddressLine(1);
                    placeName = placeName.replace("null,", " ");
                    placeName = placeName.replace("null", " ");
                }
                if (placeName != null && placeName.contains("null")) {
                    placeName = placeName.replace("null,", " ");
                    placeName = placeName.replace("null", " ");
                }
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append(", ");

                }
                // sb.append(address.getLocality()).append(", ");
                sb.append(address.getCountryName());
                if (address.getLocality() != null
                        && !address.getLocality().equalsIgnoreCase("null")) {
                    placeAdd = address.getLocality() + ", "
                            + address.getCountryName();
                } else {
                    placeAdd = address.getCountryName();
                }
                if (placeAdd != null && placeAdd.contains("null")) {
                    placeAdd = placeAdd.replace("null,", " ");
                    placeAdd = placeAdd.replace("null", " ");
                }
                if (placeAdd.contains(placeName)) {
                    placeAdd.replace(placeName, "");
                } else if (placeName.contains(placeAdd)) {
                    placeName.replace(placeAdd, "");
                }
                if (AppDelegate.isValidString(placeName) && AppDelegate.isValidString(placeAdd)) {
                    strAddress = placeName + ", " + placeAdd;
                } else if (AppDelegate.isValidString(placeName)) {
                    strAddress = placeName;
                } else if (AppDelegate.isValidString(placeAdd)) {
                    strAddress = placeAdd;
                }
                StringBuilder place = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    place.append(address.getAddressLine(i)).append(", ");
                    if (i == 1) {
                        break;
                    }
                }
                city = address.getLocality();
                country = address.getCountryName();
                postalCode = address.getPostalCode();
                state = address.getAdminArea();
                loc = place.toString();
                result = sb.toString();
            }
        } catch (IOException e) {
            AppDelegate.LogE(e);
        } finally {
            AppDelegate.LogE("Finally called for LocationAddress");
            Message message = Message.obtain();
            message.setTarget(handler);
            message.what = 2;
            Bundle bundle = new Bundle();
            bundle.putDouble(Tags.LAT, latitude);
            bundle.putDouble(Tags.LNG, longitude);
            bundle.putString(Tags.PLACE_NAME, placeName);
            bundle.putString(Tags.Location, strAddress);
            bundle.putString(Tags.PLACE_ADD, placeAdd);
            bundle.putString(Tags.STATE, state);
            bundle.putString(Tags.ADDRESS, result);
            bundle.putString(Tags.city_param, city);
            bundle.putString(Tags.country_param, country);
            bundle.putString(Tags.postalCode, postalCode);
            message.setData(bundle);
            message.sendToTarget();
        }
    }

    public static void getLocationFromReverseGeocoder(final String string, final Context context, final Handler handler, final int pickUp) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                if (context == null || handler == null) {
                    return;
                }
                setNullValue();
                execute_ReverseGeocoder(string, context, handler, pickUp);
            }
        };
        thread.start();
    }

    public static void getLocationFromReverseGeocoder(final String string, final Context context, final Handler handler)
    {
        Thread thread = new Thread() {
            @Override
            public void run() {
                if (context == null || handler == null) {
                    return;
                }
                setNullValue();
                execute_ReverseGeocoder(string, context, handler);
            }
        };
        thread.start();
    }

    public static void execute_ReverseGeocoder(final String location, final Context context, final Handler handler, int pickUp) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocationName(location, 1);
            AppDelegate.LogT("addressList location = " + location + ", addressList size = " + addressList.size());
            if (addressList != null && addressList.size() > 0)
            {
                Address address = addressList.get(0);
                AppDelegate.LogUR("addressList = " + address);
                StringBuilder sb = new StringBuilder();
                placeName = address.getAddressLine(0);
                if (address.getAddressLine(1) != null && !address.getAddressLine(1).equalsIgnoreCase("null") && address.getAddressLine(0).length() > 0) {
                    placeName = placeName + ", " + address.getAddressLine(1);
                    placeName = placeName.replace("null,", " ");
                    placeName = placeName.replace("null", " ");
                }
                if (placeName != null && placeName.contains("null")) {
                    placeName = placeName.replace("null,", " ");
                    placeName = placeName.replace("null", " ");
                }
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append(", ");
                }
                // sb.append(address.getLocality()).append(", ");
                sb.append(address.getCountryName());
                if (address.getLocality() != null
                        && !address.getLocality().equalsIgnoreCase("null")) {
                    placeAdd = address.getLocality() + ", "
                            + address.getCountryName();
                } else {
                    placeAdd = address.getCountryName();
                }
                if (placeAdd != null && placeAdd.contains("null")) {
                    placeAdd = placeAdd.replace("null,", " ");
                    placeAdd = placeAdd.replace("null", " ");
                }
                if (placeAdd.contains(placeName)) {
                    placeAdd.replace(placeName, "");
                } else if (placeName.contains(placeAdd)) {
                    placeName.replace(placeAdd, "");
                }

                latitude = address.getLatitude();
                longitude = address.getLongitude();
                StringBuilder place = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    place.append(address.getAddressLine(i)).append(", ");
                    if (i == 1) {
                        break;
                    }
                }
                city = address.getLocality();
                country = address.getCountryName();
                postalCode = address.getPostalCode();
                state = address.getAdminArea();
                loc = place.toString();
                result = sb.toString();
                // result=loc+","+city+","+state+" ,"+country+" ,"+postalCode;
            }
        } catch (IOException e) {
            AppDelegate.LogE(e);
        } finally {
            AppDelegate.LogE("Finally called for LocationAddress");
            Message message = Message.obtain();
            message.setTarget(handler);
                message.what = 9;
            Bundle bundle = new Bundle();
            bundle.putDouble(Tags.latitude, latitude);
            bundle.putDouble(Tags.longitude, longitude);

            bundle.putString(Tags.PLACE_NAME, placeName);
            bundle.putString(Tags.Location, placeName);
            bundle.putString(Tags.PLACE_ADD, placeAdd);
            bundle.putString(Tags.STATE, state);
            bundle.putString(Tags.ADDRESS, result);
            bundle.putString(Tags.city_param, city);
            bundle.putString(Tags.country_param, country);
            bundle.putString(Tags.postalCode, postalCode);
            bundle.putString(Tags.forLatlng, Tags.forLatlng);
            message.setData(bundle);
            message.sendToTarget();
        }
    }

    public static void execute_ReverseGeocoder(final String location, final Context context, final Handler handler)
    {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try
        {
            List<Address> addressList = geocoder.getFromLocationName(location, 1);

            AppDelegate.LogE("addressList location = " + location + ", addressList size = " + addressList.size());

            if (addressList != null && addressList.size() > 0)
            {
                AppDelegate.LogE("lng ===== execute_ReverseGeocoder");

                Address address = addressList.get(0);
                AppDelegate.LogUR("addressList = " + address);
                StringBuilder sb = new StringBuilder();
                placeName = address.getAddressLine(0);
                if (address.getAddressLine(1) != null && !address.getAddressLine(1).equalsIgnoreCase("null") && address.getAddressLine(0).length() > 0) {
                    placeName = placeName + ", " + address.getAddressLine(1);
                    placeName = placeName.replace("null,", " ");
                    placeName = placeName.replace("null", " ");
                }
                if (placeName != null && placeName.contains("null"))
                {
                    placeName = placeName.replace("null,", " ");
                    placeName = placeName.replace("null", " ");
                }
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++)
                {
                    sb.append(address.getAddressLine(i)).append(", ");
                }
                // sb.append(address.getLocality()).append(", ");
                sb.append(address.getCountryName());
                if (address.getLocality() != null
                        && !address.getLocality().equalsIgnoreCase("null"))
                {
                    placeAdd = address.getLocality() + ", "
                            + address.getCountryName();
                }
                else
                {
                    placeAdd = address.getCountryName();
                }
                if (placeAdd != null && placeAdd.contains("null"))
                {
                    placeAdd = placeAdd.replace("null,", " ");
                    placeAdd = placeAdd.replace("null", " ");
                }
                if (placeAdd.contains(placeName))
                {
                    placeAdd.replace(placeName, "");
                }
                else if (placeName.contains(placeAdd))
                {
                    placeName.replace(placeAdd, "");
                }

                latitude = address.getLatitude();
                longitude = address.getLongitude();

                StringBuilder place = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++)
                {
                    place.append(address.getAddressLine(i)).append(", ");
                    if (i == 1)
                    {
                        break;
                    }
                }
                city = address.getLocality();
                country = address.getCountryName();
                postalCode = address.getPostalCode();
                state = address.getAdminArea();
                loc = place.toString();
                result = sb.toString();
                // result=loc+","+city+","+state+" ,"+country+" ,"+postalCode;
            }
        }
        catch (IOException e)
        {
            AppDelegate.LogE(e);
        } finally {
            AppDelegate.LogE("Finally called for LocationAddress");
            Message message = Message.obtain();
            message.setTarget(handler);
            message.what = 9;
            Bundle bundle = new Bundle();
            bundle.putDouble(Tags.latitude, latitude);
            bundle.putDouble(Tags.longitude, longitude);

            bundle.putString(Tags.PLACE_NAME, placeName);
            bundle.putString(Tags.Location, placeName);
            bundle.putString(Tags.PLACE_ADD, placeAdd);
            bundle.putString(Tags.STATE, state);
            bundle.putString(Tags.ADDRESS, result);
            bundle.putString(Tags.city_param, city);
            bundle.putString(Tags.country_param, country);
            bundle.putString(Tags.postalCode, postalCode);
            bundle.putString(Tags.forLatlng, Tags.forLatlng);
            message.setData(bundle);
            message.sendToTarget();
        }
    }


}
