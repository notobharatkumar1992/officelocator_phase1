package com.officelocator.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class OfficeLocatorDatabase {

    // Database name
    public static String DATABASE_NAME = "OfficeLocatorDatabase";
    // Database version
    private static final int DATABASE_VERSION = 1;

    // UserInfo table declaration
    private static final String TABLE_NAME_UserInfo = "UserInfo";
    // insert statement for UserInfo table
    private static final String insert_UserInfo_Table = "insert into " + TABLE_NAME_UserInfo + "" + "(userName,password,userId)values(?,?,?)";
    // Favorite table declaration
    private static final String TABLE_NAME_FavoriteList = "FavoriteLis";
    // insert statement for Favorite table
    private static final String insert_FavoriteList_Table = "insert into " + TABLE_NAME_FavoriteList + "" + "(favoriteList)values(?)";

    private static final String TABLE_NAME_FavoriteDetail = "FavoriteDetail";
    // insert statement for Favorite table
    private static final String insert_FavoriteDetail_Table = "insert into " + TABLE_NAME_FavoriteDetail + "" + "(favoriteId,favoriteData)values(?,?)";

    // TAG to be used in all logs
    final String TAG = "Database of:";

    // Database helper declaration
    private static OpenHelper sOpenHelper;

    // SQLite database declaration
    private static SQLiteDatabase sSqLiteDatabase;

    // SQLite statement declaration
    private static SQLiteStatement sInsertStmtUserInfoTable;
    private static SQLiteStatement sInsertStmtFavoriteListTable;
    private static SQLiteStatement sInsertStmtFavoriteDetailTable;

    private static OfficeLocatorDatabase sRapidCommDatabase = new OfficeLocatorDatabase();

    /* Static 'instance' method */
    public static OfficeLocatorDatabase getInstance(Context context) {
        if (sRapidCommDatabase == null) {
            sRapidCommDatabase = new OfficeLocatorDatabase();
        }

        try {
            sOpenHelper = new OpenHelper(context);
            sSqLiteDatabase = sOpenHelper.getWritableDatabase();
            sInsertStmtUserInfoTable = sSqLiteDatabase.compileStatement(insert_UserInfo_Table);
            sInsertStmtFavoriteListTable = sSqLiteDatabase.compileStatement(insert_FavoriteList_Table);
            sInsertStmtFavoriteDetailTable = sSqLiteDatabase.compileStatement(insert_FavoriteDetail_Table);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sRapidCommDatabase;
    }

    // To check that userInfo is exist in UserInfo table or not
    public Boolean IsUserInfoExist() {
        int count = 0;
        Boolean isAvalable = false;
        String selectQuery = "SELECT * FROM " + TABLE_NAME_UserInfo;
        Cursor cursor = sSqLiteDatabase.rawQuery(selectQuery, null);
        count = cursor.getCount();
        isAvalable = count > 0;
        return isAvalable;
    }

    // To check that userInfo is exist in UserInfo table or not
    public Boolean IsValidUserExist(String userName,String password) {
        int count = 0;
        Boolean isAvalable = false;
        String selectQuery = "SELECT * FROM " + TABLE_NAME_UserInfo + " WHERE userName= '" + userName + "' AND password= '" + password + "'";
        Cursor cursor = sSqLiteDatabase.rawQuery(selectQuery, null);
        count = cursor.getCount();
        isAvalable = count > 0;
        return isAvalable;
    }

    // To check that Favorite is exist in Favorite table or not
    public Boolean IsFavoriteListExist() {
        int count = 0;
        Boolean isAvalable = false;
        String selectQuery = "SELECT * FROM " + TABLE_NAME_FavoriteList;
        Cursor cursor = sSqLiteDatabase.rawQuery(selectQuery, null);
        count = cursor.getCount();
        isAvalable = count > 0;
        return isAvalable;
    }

    // To check that product is exist in ProductCart table or not
    public Boolean IsFavoriteDetailExist(String favoriteId) {
        int count = 0;
        Boolean isAvalable = false;
        String selectQuery = "SELECT * FROM " + TABLE_NAME_FavoriteDetail + " WHERE favoriteId= '" + favoriteId + "'";
        Cursor cursor = sSqLiteDatabase.rawQuery(selectQuery, null);
        count = cursor.getCount();
        isAvalable = count > 0;
        return isAvalable;
    }

    // To insert records in UserInfo table
    public void insertUserInfo(String userName, String password, String userId) {
        Log.e("in insert", "in  insert");
        try {
            sInsertStmtUserInfoTable.bindString(1, userName);
            sInsertStmtUserInfoTable.bindString(2, password);
            sInsertStmtUserInfoTable.bindString(3, userId);
            sInsertStmtUserInfoTable.executeInsert();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    // To insert records in FavoriteList table
    public void insertFavoriteList(String favoriteList) {
        Log.e("in insert", "in  insert");
        try {
            sInsertStmtFavoriteListTable.bindString(1, favoriteList);
            sInsertStmtFavoriteListTable.executeInsert();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    // To insert records in FavoriteList table
    public void insertFavoriteData(String favoriteId,String favoriteData) {
        Log.e("in insert", "in  insert");
        try {
            sInsertStmtFavoriteDetailTable.bindString(1, favoriteId);
            sInsertStmtFavoriteDetailTable.bindString(2, favoriteData);
            sInsertStmtFavoriteDetailTable.executeInsert();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    // To get all UserInfo from UserInfo table
    public String GetUserInfo(String userName,String password) {
        String jsonResponse = "";
        String selectQuery = "SELECT userId FROM " + TABLE_NAME_UserInfo + " WHERE userName= '" + userName + "' AND password= '" + password + "'";
        Cursor cursor = sSqLiteDatabase.rawQuery(selectQuery, null);
        JSONObject jObject = new JSONObject();
        if (cursor.moveToFirst()) {
            do {
                try {
                    if (cursor.getString(cursor.getColumnIndex("userId")) == null) {
                        jObject.put("userId", "");
                    } else {
                        jObject.put("userId", cursor.getString(cursor.getColumnIndex("userId")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());

            try {
                jsonResponse = jObject.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return jsonResponse;
    }

    // To get all the FavoriteList from FavoriteList table
    public String GetAllFavoriteList() {
        String jsonResponse = "";
        String selectQuery = "SELECT favoriteList FROM " + TABLE_NAME_FavoriteList;
        Cursor cursor = sSqLiteDatabase.rawQuery(selectQuery, null);
        JSONObject jObject = new JSONObject();
        if (cursor.moveToFirst()) {
            do {
                try {
                    if (cursor.getString(cursor.getColumnIndex("favoriteList")) == null) {
                        jObject.put("favoriteList", "");
                    } else {
                        jObject.put("favoriteList", cursor.getString(cursor.getColumnIndex("favoriteList")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());

            try {
               jsonResponse = jObject.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonResponse;
    }

    public String GetFavoriteDetailByFavoriteId(String favoriteId) {
        String jsonResponse = "";
        String selectQuery = "SELECT favoriteData FROM " + TABLE_NAME_FavoriteDetail + " WHERE favoriteId= '" + favoriteId + "'";
        Cursor cursor = sSqLiteDatabase.rawQuery(selectQuery, null);
        JSONObject jObject = new JSONObject();
        if (cursor.moveToFirst()) {
            do {
                try {
                    if (cursor.getString(cursor.getColumnIndex("favoriteData")) == null) {
                        jObject.put("favoriteData", "");
                    } else {
                        jObject.put("favoriteData", cursor.getString(cursor.getColumnIndex("favoriteData")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());

            try {
                jsonResponse = jObject.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return jsonResponse;
    }

    public void deleteAllUserInfo() {
        sSqLiteDatabase.delete(TABLE_NAME_UserInfo, null, null);
    }

    public void deleteAllFavoriteList() {
        sSqLiteDatabase.delete(TABLE_NAME_FavoriteList, null, null);
    }

    // delete Product from cart of selected product id
    public void DeleteFavoriteDetailByFavoriteId(String favoriteId) {
        String where = "favoriteId= '" + favoriteId + "' ";
        sSqLiteDatabase.delete(TABLE_NAME_FavoriteDetail, where, null);
        Log.e(TAG, "Delete favoriteId" + favoriteId);
    }

    /**
     * To Create the database when user launch the application for first time
     */
    private static class OpenHelper extends SQLiteOpenHelper {
        OpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // Create Table
            Log.e("in on create", "in on create");
            db.execSQL("CREATE TABLE " + TABLE_NAME_UserInfo + " (id INTEGER PRIMARY KEY,userName Text,password Text,userId Text)");
            db.execSQL("CREATE TABLE " + TABLE_NAME_FavoriteList + " (Id INTEGER PRIMARY KEY,favoriteList Text)");
            db.execSQL("CREATE TABLE " + TABLE_NAME_FavoriteDetail + " (Id INTEGER PRIMARY KEY,favoriteId Text,favoriteData Text)");
        }

        /**
         * This method is used when DataBase version is changed
         * user launch the application it checks that database version
         * user have is different from current version if it's different then drop all the tables
         * and create all the tables once again and upgrade the database
         *
         * @param db         object of database
         * @param oldVersion old version number
         * @param newVersion current version number
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w("Example", "Upgrading database, this will drop tables and recreate.");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_UserInfo);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_FavoriteList);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_FavoriteDetail);
            onCreate(db);
        }
    }
}
