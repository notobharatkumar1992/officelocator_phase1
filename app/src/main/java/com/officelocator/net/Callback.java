package com.officelocator.net;

import com.officelocator.AppDelegate;

import retrofit.RetrofitError;

public abstract class Callback<T> implements retrofit.Callback<T> {
    public abstract void failure(RestError restError);

    @Override
    public void failure(RetrofitError error) {
        try {
            RestError restError = (RestError) error.getBodyAs(RestError.class);
            if (restError != null)
                failure(restError);
            else {
                failure(new RestError(error.getMessage()));
            }
            if (AppDelegate.mInstance != null)
                AppDelegate.showToast(AppDelegate.mInstance, "Bad server response.");
        } catch (Exception e) {
            AppDelegate.LogE(e);
            failure(new RestError("Please try again later."));
        }
    }
}