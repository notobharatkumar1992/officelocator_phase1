package com.officelocator.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.officelocator.R;
import com.officelocator.model.AnnouncementListData;

import java.util.ArrayList;

/**
 * Created by Bharat on 02-Jan-17.
 */
public class AnnuncementHorizontalAdapter extends RecyclerView.Adapter<AnnuncementHorizontalAdapter.MyViewHolder> {

    ArrayList<AnnouncementListData.list> AnnouncementArrayList;
    Context context;
    private AdapterView.OnItemClickListener itemClickListener;

    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();


    public AnnuncementHorizontalAdapter(ArrayList<AnnouncementListData.list> AnnouncementArrayList, Context context, AdapterView.OnItemClickListener itemClickListener) {
        this.AnnouncementArrayList = AnnouncementArrayList;
        this.context = context;
        this.itemClickListener = itemClickListener;
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img, img_loading;

        public MyViewHolder(View view) {
            super(view);
            img = (ImageView) view.findViewById(R.id.xml_full_img_below_middleimg);
            img_loading = (ImageView) view.findViewById(R.id.img_loading);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_xml_row_full_image_view, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.img_loading.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
        frameAnimation.setCallback(holder.img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();

//        Picasso.with(context).load(AnnouncementArrayList.get(position).logo)
//                .error(R.drawable.sample_bg)
//                .placeholder(R.drawable.sample_bg)
//                .into(holder.img);

        imageLoader.loadImage(AnnouncementArrayList.get(position).thumb, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                holder.img.setImageDrawable(context.getResources().getDrawable(R.drawable.sample_bg));
                holder.img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                holder.img.setImageBitmap(bitmap);
                holder.img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                holder.img.setImageDrawable(context.getResources().getDrawable(R.drawable.sample_bg));
                holder.img_loading.setVisibility(View.GONE);
            }
        });
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(null, null, position, position);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return AnnouncementArrayList.size();
    }
}