package com.officelocator.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.officelocator.R;
import com.officelocator.model.FavoriteListData;
import com.officelocator.util.StorePreferences;

import java.util.ArrayList;

public class FavoriteListAdapter extends BaseAdapter {

    // ArrayList declaration
    ArrayList<FavoriteListData.list> mFavoriteListArrayList;

    // Global variable declaration
    private Activity mActivity;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    public FavoriteListAdapter(Activity mActivity, ArrayList<FavoriteListData.list> favoriteArrayList) {
        this.mActivity = mActivity;
        mFavoriteListArrayList = favoriteArrayList;
        imageLoader.init(ImageLoaderConfiguration.createDefault(mActivity));
    }

    @Override
    public int getCount() {
        return mFavoriteListArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mFavoriteListArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder mViewHolder;

        if (convertView == null) {
            convertView = mActivity.getLayoutInflater().inflate(R.layout.termslist_row, null);
            mViewHolder = new ViewHolder();
            mViewHolder.textViewBranchName = (TextView) convertView.findViewById(R.id.textViewBranchName);
            mViewHolder.textViewCompanyName = (TextView) convertView.findViewById(R.id.textViewCompanyName);
            mViewHolder.termsItemImageView = (ImageView) convertView.findViewById(R.id.termsItemImageView);
            mViewHolder.rl_item = (RelativeLayout) convertView.findViewById(R.id.rl_item);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        if (StorePreferences.getInstance().getLanguage().equals("en")) {
            mViewHolder.textViewBranchName.setText(mFavoriteListArrayList.get(position).name);
        } else {
            mViewHolder.textViewBranchName.setText(mFavoriteListArrayList.get(position).name_spanish);
        }

        mViewHolder.textViewCompanyName.setText(mFavoriteListArrayList.get(position).address + "," +
                mFavoriteListArrayList.get(position).address2);
        imageLoader.loadImage(mFavoriteListArrayList.get(position).logo, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                mViewHolder.termsItemImageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.sample_bg));
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                mViewHolder.termsItemImageView.setImageBitmap(bitmap);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
//        Picasso.with(mActivity)
//                .load(mFavoriteListArrayList.get(position).logo)
//                .error(R.drawable.sample_bg)
//                .placeholder(R.drawable.sample_bg)
//                .into(mViewHolder.termsItemImageView);

        mViewHolder.rl_item.setBackgroundColor(Color.parseColor(mFavoriteListArrayList.get(position).category_color));
        mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
        mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));

//        switch (mFavoriteListArrayList.get(position).value) {
//            case 0:
//                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_1));
//                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
//                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));
//                break;
//            case 1:
//                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_2));
//                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
//                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));
//                break;
//            case 2:
//                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_3));
//                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.tiles_color_2));
//                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.tiles_color_2));
//                break;
//            case 3:
//                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_4));
//                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.tiles_color_2));
//                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.tiles_color_2));
//                break;
//            case 4:
//                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_5));
//                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
//                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));
//                break;
////            case 5:
////                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_6));
////                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
////                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));
////                break;
//        }

        return convertView;
    }

    public class ViewHolder {
        TextView textViewBranchName, textViewCompanyName;
        ImageView termsItemImageView;
        RelativeLayout rl_item;

       /* public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }*/
    }

   /* @Override
    protected void finalize() throws Throwable {
        super.finalize();
        mStorePref = null;
        mVcrDatabase = null;
    }*/
}
