package com.officelocator.adapters;

import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.model.ReviewModel;

import java.util.ArrayList;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class ReviewsListAdapter extends RecyclerView.Adapter<ReviewsListAdapter.ViewHolder>
{

    private ArrayList<ReviewModel> mainArrayList;
    private View v;
    private FragmentActivity context;
    public int action;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_item_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        final ReviewModel orderModel = mainArrayList.get(position);
        try
        {
            holder.txt_c_name.setText(orderModel.full_name);
            holder.txt_c_feedback.setText(orderModel.review);
            holder.rb_rating.setRating(Float.parseFloat(orderModel.rating + ""));
            String str_placed_on = orderModel.created.substring(0, orderModel.created.indexOf("T"));
            holder.txt_c_rated_on.setText(str_placed_on);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public ReviewsListAdapter(FragmentActivity context, ArrayList<ReviewModel> mainArrayList)
    {
        this.context = context;
        this.mainArrayList = mainArrayList;
    }

    @Override
    public int getItemCount()
    {
        return mainArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_c_name, txt_c_rated_on, txt_c_feedback, txt_c_services;
        RatingBar rb_rating;
        RelativeLayout main_containt;
        LinearLayout ll_order_detail;

        public ViewHolder(View itemView)
        {
            super(itemView);
            txt_c_name = (TextView) itemView.findViewById(R.id.txt_c_name);
            txt_c_rated_on = (TextView) itemView.findViewById(R.id.txt_c_rated_on);
            txt_c_feedback = (TextView) itemView.findViewById(R.id.txt_c_feedback);
            txt_c_services = (TextView) itemView.findViewById(R.id.txt_c_services);
            rb_rating = (RatingBar) itemView.findViewById(R.id.rb_rating);
            LayerDrawable stars = (LayerDrawable) rb_rating.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(0).setColorFilter(context.getResources().getColor(R.color.grey_500), PorterDuff.Mode.SRC_ATOP);
            txt_c_services.setVisibility(View.GONE);
        }
    }
}