package com.officelocator.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.officelocator.AppDelegate;
import com.officelocator.Constants.Tags;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.database.OfficeLocatorDatabase;
import com.officelocator.fragments.BranchDetailLocationDirectionMapFragment;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.CircleImageView;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.FlowLayout;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import carbon.widget.ImageView;
import carbon.widget.TextView;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class BranchDetailsActivity extends AppCompatActivity implements View.OnClickListener
{
    public Activity mActivity;
    private TextView BrandNameTextView;
    private TextView categoryNameTextView;
    private TextView mapAddressTextView;
    private TextView phoneNumberTextView;
    private TextView workingHoursTextView;
    private CircleImageView detailItemImageView;
    private ImageView favoriteImageView;
    private FlowLayout servicesFlowLayout;
    private FlowLayout productFlowLayout;
    private FrameLayout location_direction_map_container;
    private ProgressDialog mProgressDialog;

    private int int_color = 0;
    private String category_color;
    private String Id;
    private String BranchId;
    private String favorite_status;
    private String latitude;
    private String longitude;
    private String pinLogo;
    private Boolean favoriteStatus = false;

    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        mActivity = this;
        set_locale(StorePreferences.getInstance().getLanguage());
        setContentView(R.layout.new_branch_detail_activity);
        // Initialization
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        initView();
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

    private void setValues()
    {
        if (AppDelegate.isValidString(category_color))
        {
            findViewById(R.id.ll_background).setBackgroundColor(Color.parseColor(category_color + ""));
        }
    }


    private void initView()
    {
        ((carbon.widget.TextView) findViewById(R.id.txt_c_createOneTextView)).setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);

        Id = getIntent().getExtras().getString("Id");
        category_color = getIntent().getExtras().getString("category_color");
        AppDelegate.LogT("BranchDetailsActivity =>  category_color = " + category_color);
        BranchId = getIntent().getExtras().getString("BranchId");
        int_color = getIntent().getExtras().getInt("color");
        AppDelegate.LogT("Received at detail = " + int_color);
        if (AppDelegate.isValidString(category_color))
        {
            findViewById(R.id.ll_background).setBackgroundColor(Color.parseColor(category_color + ""));
        }

        BrandNameTextView = (TextView) findViewById(R.id.BrandNameTextView);
        categoryNameTextView = (TextView) findViewById(R.id.categoryNameTextView);
        categoryNameTextView.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);
        mapAddressTextView = (TextView) findViewById(R.id.mapAddressTextView);
        phoneNumberTextView = (TextView) findViewById(R.id.phoneNumberTextView);
        workingHoursTextView = (TextView) findViewById(R.id.workingHoursTextView);
        detailItemImageView = (CircleImageView) findViewById(R.id.detailItemImageView);
        favoriteImageView = (ImageView) findViewById(R.id.favoriteImageView);
        servicesFlowLayout = (FlowLayout) findViewById(R.id.servicesFlowLayout);
        productFlowLayout = (FlowLayout) findViewById(R.id.productFlowLayout);
        location_direction_map_container = (FrameLayout) findViewById(R.id.location_direction_map_container);
        // Initialization
        mProgressDialog = new ProgressDialog(BranchDetailsActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        if (ConnectionDetector.getInstance().isConnectingToInternet())
        {
            getBranchDetailApiCall();
        }
        else
        {
            //ConnectionDetector.getInstance().show_alert(BranchDetailsActivity.this);
            GetDetailFromLocalDataBase();
        }

        location_direction_map_container.setOnClickListener(this);
        findViewById(R.id.txt_c_view_rating).setOnClickListener(this);
    }

    private void getBranchDetailApiCall()
    {
        mProgressDialog.show();
        SingletonRestClient.get().getBranchDetail(Id, StorePreferences.getInstance().get_UseId(), StorePreferences.getInstance().getLanguage(), new Callback<Response>()
        {
            @Override
            public void failure(RestError restError)
            {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response, Response response2)
            {

                mProgressDialog.dismiss();
                try
                {
                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true"))
                    {

                        if (OfficeLocatorDatabase.getInstance(BranchDetailsActivity.this).IsFavoriteDetailExist(Id))
                        {
                            OfficeLocatorDatabase.getInstance(BranchDetailsActivity.this).DeleteFavoriteDetailByFavoriteId(Id);
                            OfficeLocatorDatabase.getInstance(BranchDetailsActivity.this).insertFavoriteData(Id, obj_json.toString());
                        }
                        else
                        {
                            OfficeLocatorDatabase.getInstance(BranchDetailsActivity.this).insertFavoriteData(Id, obj_json.toString());
                        }

                        JSONObject list = obj_json.getJSONObject("list");
                            /*GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            BranchDetailData.list mBranchDetailData = gson.fromJson(String.valueOf(
                                    new String(((TypedByteArray) response.getBody()).getBytes())), BranchDetailData.list.class);
*/
                        BrandNameTextView.setText(list.getString("name"));
                        categoryNameTextView.setText(list.getString("category_name"));

                        if (list.getString("address2") != null)
                        {
                            mapAddressTextView.setText(list.getString("address2"));
                        }

                        latitude = list.getString("latitude");
                        longitude = list.getString("longitude");
                        pinLogo = list.getString("logo");

                        if (list.has("schedule"))
                        {
                            String schedule = list.getString("schedule").toString().replace("[", "").replace("]", "").replace("\"", "");
                            String[] scheduleDate = schedule.split(",");
                            String schedule_Date = "";
                            /*for (int k =0; k<scheduleDate.length;k++){
                                Date startDate = null;
                                DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                                try {
                                    startDate = df.parse(scheduleDate[k]);
                                    String newDateString = df.format(startDate);
                                    System.out.println(newDateString);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Locale spanish = new Locale("es", "ES");
                                schedule_Date += formatTime(startDate,spanish) +"\n";
                            }*/
                            for (int k = 0; k < scheduleDate.length; k++)
                            {
                                schedule_Date += scheduleDate[k] + "\n";
                            }
                            workingHoursTextView.setText(schedule_Date);
                        }

                        String contact_no = list.getString("contact_no").toString().replace("[", "").replace("]", "").replace("\"", "");
                        String[] contactNo = contact_no.split(",");
                        String contactNumber = "";
                        for (int k = 0; k < contactNo.length; k++)
                        {
                            contactNumber += contactNo[k] + " / ";
                        }
                        phoneNumberTextView.setText(contactNumber);
                        Linkify.addLinks(phoneNumberTextView, Patterns.PHONE, "tel:");
                        phoneNumberTextView.setLinkTextColor(getResources().getColor(R.color.primary_text));

                        AppDelegate.LogT("logo => " + list.getString("logo"));
                        imageLoader.loadImage(list.getString("logo"), options, new ImageLoadingListener()
                        {
                            @Override
                            public void onLoadingStarted(String imageUri, View view)
                            {
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason)
                            {
                                detailItemImageView.setImageDrawable(getResources().getDrawable(R.drawable.sample_bg));
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap)
                            {
                                detailItemImageView.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view)
                            {

                            }
                        });

//                        Picasso.with(BranchDetailsActivity.this)
//                                .load(list.getString("logo"))
//                                .error(R.drawable.sample_bg)
//                                .placeholder(R.drawable.sample_bg)
//                                .into(detailItemImageView);
                        favorite_status = list.getString("favorite_status");

                        if (favorite_status.equals("0"))
                        {
                            favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.fav_unchecked));
                            favoriteStatus = false;
                        }
                        else
                        {
                            favoriteStatus = true;
                            favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.fav_checked));
                        }

                        favoriteImageView.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                if (!favoriteStatus)
                                {
                                    favoriteStatus = true;
                                    favorite_status = "1";
                                    favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.fav_checked));
                                }
                                else
                                {
                                    favoriteStatus = false;
                                    favorite_status = "0";
                                    favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.fav_unchecked));
                                }

                                if (ConnectionDetector.getInstance().isConnectingToInternet())
                                {
                                    UpdateFavoriteStatusApiCall();
                                }
                                else
                                {
                                    ConnectionDetector.getInstance().show_alert(BranchDetailsActivity.this);
                                }

                            }
                        });

                        JSONArray product = list.getJSONArray("product");
                        if (product.length() > 0)
                        {
                            Log.i("product", "=" + product.length());

                            ArrayList<String> productName = new ArrayList<String>();
                            for (int j = 0; j < product.length(); j++)
                            {
                                JSONObject jsonObject = product.getJSONObject(j);
                                productName.add(jsonObject.getString("name"));
                            }

                            try
                            {
                                for (int l = 0; l < productName.size(); l++)
                                {
                                    StringBuilder mStrBuilder = new StringBuilder();
                                    mStrBuilder.append(" ");
                                    mStrBuilder.append(productName.get(l));
                                    mStrBuilder.append(" ");
//                                    View view = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.flow_text_item, null);
//                                    final TextView tt = (TextView) view.findViewById(R.id.txt_c_flow);
//                                    tt.setText(mStrBuilder.toString());
//                                    tt.setClickable(true);
                                    final android.widget.TextView tt = new android.widget.TextView(BranchDetailsActivity.this);
                                    tt.setText("> " + mStrBuilder.toString());
                                    tt.setTextColor(Color.WHITE);
                                    tt.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD);
                                    tt.setTextSize(13);
                                    tt.setPadding(5, 5, 5, 5);
                                    tt.setBackgroundColor(Color.TRANSPARENT);
                                    tt.setClickable(true);
                                    FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
                                    params.rightMargin = 30;
                                    params.setMargins(5, 5, 5, 5);
                                    tt.setLayoutParams(params);
                                    productFlowLayout.addView(tt);
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                        try
                        {
                            Bundle bundle = new Bundle();
                            bundle.putString("destinationLatitude", latitude);
                            bundle.putString("destinationLongitude", longitude);
                            bundle.putString("pinLogo", list.getString("logo"));
                            bundle.putString("category_color", category_color);
                            bundle.putInt("color", int_color);

                            AppDelegate.LogT("send to BranchDetailOpenMapFullFragment color => " + int_color);
                            BranchDetailLocationDirectionMapFragment mBranchDetailLocationDirectionMapFragment = new BranchDetailLocationDirectionMapFragment();
                            mBranchDetailLocationDirectionMapFragment.setArguments(bundle);
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.add(R.id.location_direction_map_container, mBranchDetailLocationDirectionMapFragment).commit();

                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                        JSONArray service = list.getJSONArray("service");
                        if (service.length() > 0)
                        {
                            Log.i("service", "=" + service.length());

                            ArrayList<String> serviceName = new ArrayList<String>();
                            for (int j = 0; j < service.length(); j++)
                            {
                                JSONObject jsonObject = service.getJSONObject(j);
                                serviceName.add(jsonObject.getString("name"));
                            }

                            try
                            {
                                for (int k = 0; k < serviceName.size(); k++)
                                {
                                    StringBuilder mStrBuilder = new StringBuilder();
                                    mStrBuilder.append(" ");
                                    mStrBuilder.append(serviceName.get(k));
                                    mStrBuilder.append(" ");
//                                    View view = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.flow_text_item, null);
//                                    final TextView tt = (TextView) view.findViewById(R.id.txt_c_flow);
//                                    tt.setText("> " + mStrBuilder.toString());
//                                    tt.setTextColor(Color.WHITE);
//                                    tt.setClickable(true);
//                                    servicesFlowLayout.addView(view);

                                    final android.widget.TextView tt = new android.widget.TextView(BranchDetailsActivity.this);
                                    tt.setText("> " + mStrBuilder.toString());
                                    tt.setTextColor(Color.WHITE);
                                    tt.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD);
                                    tt.setTextSize(13);
                                    tt.setPadding(5, 5, 5, 5);
                                    tt.setBackgroundColor(Color.TRANSPARENT);
                                    tt.setClickable(true);
                                    FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
                                    params.rightMargin = 30;
                                    params.setMargins(5, 5, 5, 5);
                                    tt.setLayoutParams(params);
                                    servicesFlowLayout.addView(tt);
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }

                        setValues();
                    }
                    else
                    {
                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(BranchDetailsActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception e)
                {
                    AppDelegate.LogE(e);
                    AppDelegate.showToast(BranchDetailsActivity.this, "Bad server response.");
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });
    }

    private void UpdateFavoriteStatusApiCall()
    {
        mProgressDialog.show();//"26.849088", "75.804443"
        SingletonRestClient.get().setFavoriteUpdate(Id, StorePreferences.getInstance().get_UseId(),
                latitude, longitude, favorite_status, new Callback<Response>()
                {
                    @Override
                    public void failure(RestError restError)
                    {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void success(Response response,
                                        Response response2)
                    {
                        mProgressDialog.dismiss();

                        try
                        {
                            JSONObject obj_json = new JSONObject(
                                    new String(((TypedByteArray) response.getBody()).getBytes()));
                            String responseStatus = obj_json.getString("response");
                            if (responseStatus.equals("1"))
                            {
                                String responseMessage = obj_json.getString("Message");
                                Toast.makeText(BranchDetailsActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                String responseMessage = obj_json.getString("Message");
                                Toast.makeText(BranchDetailsActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception e)
                        {
                            // TODO Auto-generated catch block
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(BranchDetailsActivity.this, "Bad server response.");
                            //AlertDialog("Server Error! Please try again.");
                        }
                    }
                });

    }


    @Override
    public void onClick(View v)
    {
        if (v == location_direction_map_container)
        {
            try
            {
                Intent intent = new Intent(BranchDetailsActivity.this, BranchDetailLocationActivity.class);
                Log.d("Map", "Map clicked");
                Bundle bundle = new Bundle();
                bundle.putString("Id", "");
                bundle.putString("destinationLatitude", latitude);
                bundle.putString("destinationLongitude", longitude);
                bundle.putString("pinLogo", pinLogo);
                bundle.putInt("color", int_color);

                intent.putExtras(bundle);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(BranchDetailsActivity.this, false,
                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(BranchDetailsActivity.this, pairs);
                startActivity(intent, transitionActivityOptions.toBundle());
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
            }
        }
        else if (v.getId() == R.id.txt_c_view_rating)
        {
            Intent intent = new Intent(BranchDetailsActivity.this, RatingActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Tags.id, Id);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    /**
     * This Interfaces must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void GetDetailFromLocalDataBase()
    {
        if (OfficeLocatorDatabase.getInstance(BranchDetailsActivity.this).IsFavoriteDetailExist(Id))
        {
            String response = OfficeLocatorDatabase.getInstance(BranchDetailsActivity.this).GetFavoriteDetailByFavoriteId(Id);
            try
            {
                JSONObject jsonObject1 = new JSONObject(response);
                String favoriteData = jsonObject1.getString("favoriteData");
                JSONObject obj_json = new JSONObject(favoriteData);
                String responseStatus = obj_json.getString("Status");
                if (responseStatus.equals("true"))
                {

                    JSONObject list = obj_json.getJSONObject("list");
                    BrandNameTextView.setText(list.getString("name"));
                    categoryNameTextView.setText(list.getString("category_name"));
                    category_color = list.getString("category_color");
                    if (list.getString("address2") != null)
                    {
                        mapAddressTextView.setText(list.getString("address2"));
                    }
                    latitude = list.getString("latitude");
                    longitude = list.getString("longitude");

                    if (list.has("schedule"))
                    {
                        String schedule = list.getString("schedule").toString().replace("[", "").replace("]", "").replace("\"", "");
                        String[] scheduleDate = schedule.split(",");
                        String schedule_Date = "";
                        for (int k = 0; k < scheduleDate.length; k++)
                        {
                            schedule_Date += scheduleDate[k] + "\n";
                        }
                        workingHoursTextView.setText(schedule_Date);
                    }

                    String contact_no = list.getString("contact_no").toString().replace("[", "").replace("]", "").replace("\"", "");
                    String[] contactNo = contact_no.split(",");
                    String contactNumber = "";
                    for (int k = 0; k < contactNo.length; k++)
                    {
                        contactNumber += contactNo[k] + " / ";
                    }
                    phoneNumberTextView.setText(contactNumber);
                    Linkify.addLinks(phoneNumberTextView, Patterns.PHONE, "tel:");
                    phoneNumberTextView.setLinkTextColor(getResources().getColor(R.color.primary_text));
                    imageLoader.loadImage(list.getString("logo"), options, new ImageLoadingListener()
                    {
                        @Override
                        public void onLoadingStarted(String imageUri, View view)
                        {
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason)
                        {
                            if (mActivity != null)
                            {
                                detailItemImageView.setImageDrawable(getResources().getDrawable(R.drawable.sample_bg));
                            }
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap bitmap)
                        {
                            if (mActivity != null)
                            {
                                detailItemImageView.setImageBitmap(bitmap);
                            }
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view)
                        {

                        }
                    });

//                    Picasso.with(BranchDetailsActivity.this)
//                            .load(list.getString("logo"))
//                            .error(R.drawable.sample_bg)
//                            .placeholder(R.drawable.sample_bg)
//                            .into(detailItemImageView);
                    favorite_status = list.getString("favorite_status");

                    if (favorite_status.equals("0"))
                    {
                        favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.fav_unchecked));
                        favoriteStatus = false;
                    }
                    else
                    {
                        favoriteStatus = true;
                        favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.fav_checked));
                    }

                    favoriteImageView.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            if (!favoriteStatus)
                            {
                                favorite_status = "1";
                                favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.fav_checked));
                            }
                            else
                            {
                                favorite_status = "0";
                                favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.fav_unchecked));
                            }

                            if (ConnectionDetector.getInstance().isConnectingToInternet())
                            {
                                UpdateFavoriteStatusApiCall();
                            }
                            else
                            {
                                ConnectionDetector.getInstance().show_alert(BranchDetailsActivity.this);
                            }

                        }
                    });

                    JSONArray product = list.getJSONArray("product");
                    if (product.length() > 0)
                    {
                        Log.i("product", "=" + product.length());

                        ArrayList<String> productName = new ArrayList<String>();
                        for (int j = 0; j < product.length(); j++)
                        {
                            JSONObject jsonObject = product.getJSONObject(j);
                            productName.add(jsonObject.getString("name"));
                        }

                        try
                        {
                            for (int l = 0; l < productName.size(); l++)
                            {
                                StringBuilder mStrBuilder = new StringBuilder();
                                mStrBuilder.append(" ");
                                mStrBuilder.append(productName.get(l));
                                mStrBuilder.append(" ");
//                                View view = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.flow_text_item, null);
//                                final TextView tt = (TextView) view.findViewById(R.id.txt_c_flow);
//                                tt.setText(mStrBuilder.toString());
//                                tt.setClickable(true);
//                                productFlowLayout.addView(view);
                                final android.widget.TextView tt = new android.widget.TextView(BranchDetailsActivity.this);
                                tt.setText("> " + mStrBuilder.toString());
                                tt.setTextColor(Color.WHITE);
                                tt.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD);
                                tt.setTextSize(13);
                                tt.setPadding(5, 5, 5, 5);
                                tt.setBackgroundColor(Color.TRANSPARENT);
                                tt.setClickable(true);
                                FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
                                params.rightMargin = 30;
                                params.setMargins(5, 5, 5, 5);
                                tt.setLayoutParams(params);
                                servicesFlowLayout.addView(tt);
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                    try
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString("destinationLatitude", latitude);
                        bundle.putString("destinationLongitude", longitude);
                        bundle.putString("pinLogo", list.getString("logo"));
                        bundle.putInt("color", int_color);
                        AppDelegate.LogT("send to BranchDetailOpenMapFullFragment color 2 => " + int_color);

                        BranchDetailLocationDirectionMapFragment mBranchDetailLocationDirectionMapFragment = new BranchDetailLocationDirectionMapFragment();
                        mBranchDetailLocationDirectionMapFragment.setArguments(bundle);
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.add(R.id.location_direction_map_container, mBranchDetailLocationDirectionMapFragment).commit();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    JSONArray service = list.getJSONArray("service");
                    if (service.length() > 0)
                    {
                        Log.i("service", "=" + service.length());

                        ArrayList<String> serviceName = new ArrayList<String>();
                        for (int j = 0; j < service.length(); j++)
                        {
                            JSONObject jsonObject = service.getJSONObject(j);
                            serviceName.add(jsonObject.getString("name"));
                        }

                        try
                        {
                            for (int k = 0; k < serviceName.size(); k++)
                            {
                                StringBuilder mStrBuilder = new StringBuilder();
                                mStrBuilder.append(" ");
                                mStrBuilder.append(serviceName.get(k));
                                mStrBuilder.append(" ");
//                                View view = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.flow_text_item, null);
//                                final TextView tt = (TextView) view.findViewById(R.id.txt_c_flow);
//                                tt.setText(mStrBuilder.toString());
//                                tt.setTextColor(Color.WHITE);
//                                tt.setClickable(true);
//                                servicesFlowLayout.addView(view);
                                final android.widget.TextView tt = new android.widget.TextView(BranchDetailsActivity.this);
                                tt.setText("> " + mStrBuilder.toString());
                                tt.setTextColor(Color.WHITE);
                                tt.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD);
                                tt.setTextSize(13);
                                tt.setPadding(5, 5, 5, 5);
                                tt.setBackgroundColor(Color.TRANSPARENT);
                                tt.setClickable(true);
                                FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
                                params.rightMargin = 30;
                                params.setMargins(5, 5, 5, 5);
                                tt.setLayoutParams(params);
                                servicesFlowLayout.addView(tt);
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }


                }
                else
                {
                    String responseMessage = obj_json.getString("Message");
                    Toast.makeText(BranchDetailsActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                }
            }
            catch (JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
                //AlertDialog("Server Error! Please try again.");
            }
            setValues();
        }
        else
        {
            Toast.makeText(BranchDetailsActivity.this, getResources().getString(R.string.detail_branch_no_data_message), Toast.LENGTH_LONG).show();
        }
    }

   /* public static String formatTime(Date time, Locale locale){
        String timeFormat = " dd/MM/yyyy hh:mm:ss";

        SimpleDateFormat formatter;

        try {
            formatter = new SimpleDateFormat(timeFormat, locale);
        } catch(Exception e) {
            formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss", locale);
        }
        return formatter.format(time);
    }*/

    private void set_locale(String lan)
    {
        Log.d("test", "set_locale => " + lan);
        Locale locale = new Locale(lan);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mActivity = null;
    }
}
