package com.officelocator.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.adapters.AdvanceSearchListAdapter;
import com.officelocator.fragments.SearchResultMapFragment;
import com.officelocator.model.AdvanceSearchResponseListData;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by himanshu on 3/24/2016.
 */
public class SearchedResultActivity extends AppCompatActivity implements View.OnClickListener
{
    private int int_color = 0;
    private static Double latitude = 21.170240, longitude = 72.831062;
    private static String categoryId, category_color;
    private String productName;
    public double lat, lng;

    private ListView BranchListView;
    private FrameLayout map_container;
    private ProgressDialog mProgressDialog;

    private TextView txt_c_createOneTextView, favoriteErrorMessageTextView;

    ArrayList<AdvanceSearchResponseListData.list> AdvanceSearchArrayList;
    AdvanceSearchListAdapter mAdvanceSearchListAdapter;

    public SearchedResultActivity()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.new_favourite_activity);

        int_color = getIntent().getExtras().getInt("color");
        categoryId = getIntent().getExtras().getString("categoryId");
        category_color = getIntent().getExtras().getString("category_color");
        productName = getIntent().getExtras().getString("productName");
        lat = Double.parseDouble(getIntent().getStringExtra("lat"));
        lng = Double.parseDouble(getIntent().getStringExtra("lng"));

        Log.i("categoryId", categoryId + " + " + category_color);
        initView();
    }

    private void initView()
    {
        // Initialization
        txt_c_createOneTextView = (TextView) findViewById(R.id.txt_c_createOneTextView);
        txt_c_createOneTextView.setText(getResources().getString(R.string.title_search_terms));
        txt_c_createOneTextView.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);

        favoriteErrorMessageTextView = (TextView) findViewById(R.id.favoriteErrorMessageTextView);

        findViewById(R.id.txt_c_list).setOnClickListener(this);
        findViewById(R.id.txt_c_map).setOnClickListener(this);

        mProgressDialog = new ProgressDialog(SearchedResultActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        BranchListView = (ListView) findViewById(R.id.favoriteListView);
        map_container = (FrameLayout) findViewById(R.id.map_container);

        findViewById(R.id.txt_c_list).performClick();
        if (ConnectionDetector.getInstance().isConnectingToInternet())
        {
            advanceSearchApiCall();
        }
    }

    public boolean mapLoadedOnce = false;

    public void loadMap()
    {
        try
        {
            if (!mapLoadedOnce)
            {
                mapLoadedOnce = true;
                Bundle bundle = new Bundle();
                bundle.putDouble("currentLat", lat);
                bundle.putDouble("currentLong", lng);
                bundle.putString("categoryId", categoryId);
                bundle.putString("category_color", category_color);
                bundle.putInt("color", int_color);
                bundle.putParcelableArrayList("value", AdvanceSearchArrayList);
                SearchResultMapFragment mapFragment = new SearchResultMapFragment();
                mapFragment.setArguments(bundle);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.map_container, mapFragment).commit();
            }
            else
            {
                AppDelegate.LogT("HomeActivity, method loadMap, map loaded before");
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.txt_c_map)
        {
            loadMap();
            map_container.setVisibility(View.VISIBLE);
            BranchListView.setVisibility(View.GONE);
        }
        else if (v.getId() == R.id.txt_c_list)
        {
            BranchListView.setVisibility(View.VISIBLE);
            map_container.setVisibility(View.GONE);
        }
    }

    private void advanceSearchApiCall()
    {
        mProgressDialog.show();
        String radius;
        if (!StorePreferences.getInstance().getRadius().equals(""))
        {
            radius = StorePreferences.getInstance().getRadius();
        }
        else
        {
            radius = "5";
        }
        if (!AppDelegate.isValidLatLng(lat + "") || !AppDelegate.isValidLatLng(lng + ""))
        {
            lat = Double.parseDouble(StorePreferences.getInstance().getLatitude());
            lng = Double.parseDouble(StorePreferences.getInstance().getLongitude());
        }

        SingletonRestClient.get().getAdvanceSearch("", radius,
                categoryId, productName, lat + "", lng + "",
                StorePreferences.getInstance().get_UseId(),
                StorePreferences.getInstance().getLanguage(),
                new Callback<Response>()
                {
                    @Override
                    public void failure(RestError restError)
                    {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void success(Response response, Response response2)
                    {
                        mProgressDialog.dismiss();

                        try
                        {
                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                            String responseStatus = obj_json.getString("Status");
                            AppDelegate.LogE("responseStatus === " + obj_json);
                            if (responseStatus.equals("true"))
                            {
                                JSONArray list = new JSONArray(obj_json.getString("list"));

                                if (list.length() > 0)
                                {
                                    BranchListView.setVisibility(View.VISIBLE);
                                    favoriteErrorMessageTextView.setVisibility(View.GONE);
                                    AdvanceSearchArrayList = new ArrayList<>();
                                    GsonBuilder builder = new GsonBuilder();
                                    Gson gson = builder.create();

                                    AdvanceSearchResponseListData mAdvanceSearchResponseListData = gson.fromJson(String.valueOf(
                                            new String(((TypedByteArray) response.getBody()).getBytes())), AdvanceSearchResponseListData.class);
                                    AdvanceSearchArrayList = mAdvanceSearchResponseListData.list;

                                    Log.e("FavoriteListData", AdvanceSearchArrayList.size() + "==");

                                    int count = 0;
                                    for (int i = 0; i < AdvanceSearchArrayList.size(); i++)
                                    {
                                        AdvanceSearchArrayList.get(i).value = count;
//                                        AdvanceSearchArrayList.get(i).category_color = category_color;
                                        if (count == AppDelegate.TILES_COLOR_COUNT)
                                        {
                                            count = 0;
                                        }
                                        else
                                        {
                                            count++;
                                        }
                                    }

                                    mAdvanceSearchListAdapter = new AdvanceSearchListAdapter(SearchedResultActivity.this, AdvanceSearchArrayList);
                                    BranchListView.setAdapter(mAdvanceSearchListAdapter);
                                    BranchListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                                    {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                                        {
                                            Bundle bundle = new Bundle();
                                            bundle.putString("Id", AdvanceSearchArrayList.get(position).id);
                                            bundle.putString("BranchId", AdvanceSearchArrayList.get(position).category_id);
                                            bundle.putString("category_color", AdvanceSearchArrayList.get(position).category_color);
                                            bundle.putInt("color", AdvanceSearchArrayList.get(position).value);

                                            Intent intent = new Intent(SearchedResultActivity.this, BranchDetailsActivity.class);
                                            final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SearchedResultActivity.this, false,
                                                    new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                                            ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SearchedResultActivity.this, pairs);
                                            intent.putExtras(bundle);
                                            startActivity(intent, transitionActivityOptions.toBundle());
                                        }
                                    });
                                }
                            }
                            else
                            {
                                String responseMessage = obj_json.getString("Message");
                                favoriteErrorMessageTextView.setVisibility(View.VISIBLE);
                                Toast.makeText(SearchedResultActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception e)
                        {
                            // TODO Auto-generated catch block
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(SearchedResultActivity.this, "Bad server response.");
                            //AlertDialog("Server Error! Please try again.");
                        }
                    }
                });
    }

}
