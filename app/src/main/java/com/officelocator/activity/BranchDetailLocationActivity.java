package com.officelocator.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.util.CircleImageView;
import com.officelocator.util.StorePreferences;
import com.officelocator.util.WorkaroundMapFragment;

import java.util.ArrayList;

/**
 * Created by Bharat on 07/29/2016.
 */
public class BranchDetailLocationActivity extends AppCompatActivity {

    public Activity mActivity;
    private GoogleMap map_business;
    private Handler mHandler;

    private int int_color = 0;
    private String currentLatitude;
    private String currentLongitude;
    private String destinationLatitude;
    private String destinationLongitude;
    private String pinLogo;
    private Marker customMarker;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.new_map_detail);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        mActivity = this;

        currentLatitude = StorePreferences.getInstance().getLatitude();
        currentLongitude = StorePreferences.getInstance().getLongitude();

        int_color = getIntent().getExtras().getInt("color");
        destinationLatitude = getIntent().getExtras().getString("destinationLatitude");
        destinationLongitude = getIntent().getExtras().getString("destinationLongitude");
        pinLogo = getIntent().getExtras().getString("pinLogo");

        setHandler();
        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mGoogleMap) {
                map_business = mGoogleMap;
                try {
                    showMap();
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        });
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(BranchDetailLocationActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(BranchDetailLocationActivity.this);
                } else if (msg.what == 2) {
                }
            }
        };
    }

    private void showMap() {
        if (map_business == null) {
            return;
        }
        map_business.setPadding(0, AppDelegate.dpToPix(this, 100), 0, 0);
//        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        map_business.clear();
        AppDelegate.LogT("showMap called & start process for marker");
        map_business.getUiSettings().setZoomControlsEnabled(true);

        final LatLngBounds.Builder builder = LatLngBounds.builder();

        LatLng myLocation = new LatLng(Double.valueOf(currentLatitude), Double.valueOf(currentLongitude));
        builder.include(myLocation);
        map_business.addMarker(new MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.userpin)).title(BranchDetailLocationActivity.this.getResources().getString(R.string.map_user_pin_title)));

        if (!AppDelegate.isValidString(destinationLatitude) && !AppDelegate.isValidString(destinationLongitude)) {
            if (!AppDelegate.isValidString(destinationLatitude)) {
                destinationLatitude = "0.0";
            }
            if (!AppDelegate.isValidString(destinationLongitude)) {
                destinationLongitude = "0.0";
            }
        }

        LatLng destinationLocation = new LatLng(Double.valueOf(destinationLatitude), Double.valueOf(destinationLongitude));
        builder.include(destinationLocation);

        View marker = ((LayoutInflater) BranchDetailLocationActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout_demo, null);
        final CircleImageView categoryImageView = (CircleImageView) marker.findViewById(R.id.categoryRoundedImageView);
        ImageView img_marker = (ImageView) marker.findViewById(R.id.img_marker);
        switch (int_color) {
            case 0:
                img_marker.setImageResource(R.drawable.marker_1);
                break;
            case 1:
                img_marker.setImageResource(R.drawable.marker_2);
                break;
            case 2:
                img_marker.setImageResource(R.drawable.marker_3);
                break;
            case 3:
                img_marker.setImageResource(R.drawable.marker_4);
                break;
            case 4:
                img_marker.setImageResource(R.drawable.marker_5);
                break;
            default:
                img_marker.setImageResource(R.drawable.marker_1);
                break;
        }
        imageLoader.loadImage(pinLogo, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                if (mActivity != null)
                    categoryImageView.setImageDrawable(getResources().getDrawable(R.drawable.sample_bg));
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                if (mActivity != null)
                    categoryImageView.setImageBitmap(bitmap);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
//        Picasso.with(BranchDetailLocationActivity.this)
//                .load(pinLogo)
//                .error(R.drawable.sample_bg)
//                .placeholder(R.drawable.sample_bg)
//                .into(categoryImageView);

        // Bitmap bmp = getBitmapFromURL(BranchArrayList.get(i).logo);
        customMarker = map_business.addMarker(new MarkerOptions()
                .position(destinationLocation)
                .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(BranchDetailLocationActivity.this, marker))));

        // m.showInfoWindow();

        PolylineOptions polylineOptions = new PolylineOptions();

        ArrayList<LatLng> points = new ArrayList<LatLng>();

        // Setting the color of the polyline
        polylineOptions.color(Color.RED);

        // Setting the width of the polyline
        polylineOptions.width(5);

        // Adding the taped point to the ArrayList
        points.add(myLocation);
        points.add(destinationLocation);

        // Setting points of polyline
        polylineOptions.addAll(points);

        // Adding the polyline to the map
        map_business.addPolyline(polylineOptions);
        AppDelegate.LogT("showMap called & end process for marker");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (mActivity != null)
                        map_business.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1000);

    }

    private void initView() {
//        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).getMapAsync(new OnMapReadyCallback() {
//            @Override
//            public void onMapReady(GoogleMap map_business) {
//                BranchDetailLocationActivity.this.map_business = map_business;
//                showMap();
//            }
//        });

        try {
            LatLng myLocation = new LatLng(Double.valueOf(currentLatitude), Double.valueOf(currentLongitude));
            if (mActivity != null) {

                map_business.getUiSettings().setZoomControlsEnabled(true);
                map_business.addMarker(new MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.userpin)).title(BranchDetailLocationActivity.this.getResources().getString(R.string.map_user_pin_title)));
                map_business.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15));
                map_business.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

                LatLng destinationLocation = new LatLng(Double.valueOf(destinationLatitude), Double.valueOf(destinationLongitude));
                View marker = ((LayoutInflater) BranchDetailLocationActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout_demo, null);
                final CircleImageView categoryImageView = (CircleImageView) marker.findViewById(R.id.categoryRoundedImageView);
                imageLoader.loadImage(pinLogo, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        categoryImageView.setImageDrawable(getResources().getDrawable(R.drawable.sample_bg));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                        categoryImageView.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {

                    }
                });
//                Picasso.with(BranchDetailLocationActivity.this)
//                        .load(pinLogo)
//                        .error(R.drawable.sample_bg)
//                        .placeholder(R.drawable.sample_bg)
//                        .into(categoryImageView);

                // Bitmap bmp = getBitmapFromURL(BranchArrayList.get(i).logo);
                customMarker = map_business.addMarker(new MarkerOptions()
                        .position(destinationLocation)
                        .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(BranchDetailLocationActivity.this, marker))));

                // m.showInfoWindow();

                PolylineOptions polylineOptions = new PolylineOptions();

                ArrayList<LatLng> points = new ArrayList<LatLng>();

                // Setting the color of the polyline
                polylineOptions.color(Color.RED);

                // Setting the width of the polyline
                polylineOptions.width(5);

                // Adding the taped point to the ArrayList
                points.add(myLocation);
                points.add(destinationLocation);

                // Setting points of polyline
                polylineOptions.addAll(points);

                // Adding the polyline to the map
                map_business.addPolyline(polylineOptions);
            }

            final Handler handler = new Handler();
            final Runnable r = new Runnable() {
                public void run() {

                    if (BranchDetailLocationActivity.this != null) {
                        map_business.clear();
                        map_business.getUiSettings().setZoomControlsEnabled(true);
                        LatLng myLocation = new LatLng(Double.valueOf(currentLatitude), Double.valueOf(currentLongitude));
                        map_business.addMarker(new MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.userpin)).title(BranchDetailLocationActivity.this.getResources().getString(R.string.map_user_pin_title)));

                        LatLng destinationLocation = new LatLng(Double.valueOf(destinationLatitude), Double.valueOf(destinationLongitude));

                        View marker = ((LayoutInflater) BranchDetailLocationActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout_demo, null);
                        final CircleImageView categoryImageView = (CircleImageView) marker.findViewById(R.id.categoryRoundedImageView);

                        imageLoader.loadImage(pinLogo, options, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                categoryImageView.setImageDrawable(getResources().getDrawable(R.drawable.sample_bg));
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                                categoryImageView.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {

                            }
                        });

//                        Picasso.with(BranchDetailLocationActivity.this)
//                                .load(pinLogo)
//                                .error(R.drawable.sample_bg)
//                                .placeholder(R.drawable.sample_bg)
//                                .into(categoryImageView);

                        // Bitmap bmp = getBitmapFromURL(BranchArrayList.get(i).logo);
                        customMarker = map_business.addMarker(new MarkerOptions()
                                .position(destinationLocation)
                                .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(BranchDetailLocationActivity.this, marker))));

                        // m.showInfoWindow();

                        PolylineOptions polylineOptions = new PolylineOptions();

                        ArrayList<LatLng> points = new ArrayList<LatLng>();

                        // Setting the color of the polyline
                        polylineOptions.color(Color.RED);

                        // Setting the width of the polyline
                        polylineOptions.width(5);

                        // Adding the taped point to the ArrayList
                        points.add(myLocation);
                        points.add(destinationLocation);

                        // Setting points of polyline
                        polylineOptions.addAll(points);

                        // Adding the polyline to the map
                        map_business.addPolyline(polylineOptions);
                        //handler.postDelayed(this, 10000);
                    }
                }
            };
            handler.postDelayed(r, 2500);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }
}
