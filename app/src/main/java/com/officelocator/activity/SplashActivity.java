package com.officelocator.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.util.StorePreferences;

public class SplashActivity extends Activity
{

    // Global variable declaration
    protected int mSplashTime = 3000;

    private Thread mSplashTread;

    // Set window format ot RGBA_8888
    public void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // inflate layout xml file for Splash Activity
        setContentView(R.layout.activity_splash);
        //StartAnimations();
        mSplashTread = new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    synchronized (this)
                    {
                        wait(mSplashTime);
                    }
                }
                catch (InterruptedException ignored)
                {

                }
                finally
                {
                    if (!StorePreferences.getInstance().get_UseId().equals("") && StorePreferences.getInstance().getIsRememberMe())
                    {
                        Intent mainActivityIntent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(mainActivityIntent);
                        finish();
                    }
                    else
                    {
                        Intent mainActivityIntent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(mainActivityIntent);
                        finish();
                    }
                }
                try
                {
                    sleep(mSplashTime);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        };

        mSplashTread.start();
        LoginActivity.initGCM(this);
        AppDelegate.getHashKey(SplashActivity.this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if (event.getAction() == MotionEvent.ACTION_DOWN)
        {
            synchronized (mSplashTread)
            {
                mSplashTread.notifyAll();
            }
        }
        return true;
    }

    // when user press the back button then only current thread would be interrupted
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        try
        {
            mSplashTread.interrupt();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
