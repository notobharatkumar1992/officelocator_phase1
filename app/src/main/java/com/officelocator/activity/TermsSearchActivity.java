package com.officelocator.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.adapters.AdvanceSearchListAdapter;
import com.officelocator.model.AdvanceSearchResponseListData;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by himanshu on 3/24/2016.
 */
public class TermsSearchActivity extends AppCompatActivity {

    private ListView termsListView;
    private TextView termsErrorMessageTextView;
    private ProgressDialog mProgressDialog;

    ArrayList<AdvanceSearchResponseListData.list> AdvanceSearchArrayList;
    AdvanceSearchListAdapter mAdvanceSearchListAdapter;

    //Global variable declaration
    private String categoryId;
    private String category_color;
    private String postalCode;
    private String productName;
    public double lat, lng;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        AppDelegate.set_locale(this, StorePreferences.getInstance().getLanguage());
        setContentView(R.layout.new_terms_activity);
        initView();
    }

    private void initView() {
        categoryId = getIntent().getExtras().getString("categoryId");
        category_color = getIntent().getExtras().getString("category_color");
        postalCode = getIntent().getExtras().getString("postalCode");
        productName = getIntent().getExtras().getString("productName");
        lat = getIntent().getExtras().getDouble("lat");
        lng = getIntent().getExtras().getDouble("lng");

        // Initialization
        mProgressDialog = new ProgressDialog(TermsSearchActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        termsListView = (ListView) findViewById(R.id.termsListView);
        termsErrorMessageTextView = (TextView) findViewById(R.id.termsErrorMessageTextView);

        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
            advanceSearchApiCall();
        } else {
            ConnectionDetector.getInstance().show_alert(TermsSearchActivity.this);
        }
    }

    private void advanceSearchApiCall() {
        mProgressDialog.show();
        String radius = "";
        if (!StorePreferences.getInstance().getRadius().equals("")) {
            radius = StorePreferences.getInstance().getRadius();
        } else {
            radius = "5";
        }
        if (!AppDelegate.isValidLatLng(lat + "") || !AppDelegate.isValidLatLng(lng + "")) {
            lat = Double.parseDouble(StorePreferences.getInstance().getLatitude());
            lng = Double.parseDouble(StorePreferences.getInstance().getLongitude());
        }
        SingletonRestClient.get().getAdvanceSearch("", radius,
                categoryId, productName, lat + "", lng + "",
                StorePreferences.getInstance().get_UseId(),
                StorePreferences.getInstance().getLanguage(),
                new Callback<Response>() {
                    @Override
                    public void failure(RestError restError) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void success(Response response,
                                        Response response2) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject obj_json = new JSONObject(
                                    new String(((TypedByteArray) response.getBody()).getBytes()));
                            String responseStatus = obj_json.getString("Status");
                            if (responseStatus.equals("true")) {
                                JSONArray list = new JSONArray(obj_json.getString("list"));

                                if (list.length() > 0) {
                                    termsListView.setVisibility(View.VISIBLE);
                                    termsErrorMessageTextView.setVisibility(View.GONE);
                                    AdvanceSearchArrayList = new ArrayList<AdvanceSearchResponseListData.list>();
                                    GsonBuilder builder = new GsonBuilder();
                                    Gson gson = builder.create();

                                    AdvanceSearchResponseListData mAdvanceSearchResponseListData = gson.fromJson(String.valueOf(
                                            new String(((TypedByteArray) response.getBody()).getBytes())), AdvanceSearchResponseListData.class);
                                    AdvanceSearchArrayList = mAdvanceSearchResponseListData.list;

                                    Log.i("FavoriteListData", AdvanceSearchArrayList.size() + "==");
                                    int count = 0;
                                    for (int i = 0; i < AdvanceSearchArrayList.size(); i++) {
                                        AdvanceSearchArrayList.get(i).value = count;
//                                        AdvanceSearchArrayList.get(i).category_color = category_color;
                                        if (count == AppDelegate.TILES_COLOR_COUNT) {
                                            count = 0;
                                        } else {
                                            count++;
                                        }
                                    }

                                    mAdvanceSearchListAdapter = new AdvanceSearchListAdapter(TermsSearchActivity.this, AdvanceSearchArrayList);
                                    termsListView.setAdapter(mAdvanceSearchListAdapter);
                                    termsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                            Bundle bundle = new Bundle();
                                            bundle.putString("Id", AdvanceSearchArrayList.get(position).id);
                                            bundle.putString("BranchId", AdvanceSearchArrayList.get(position).category_id);
                                            bundle.putString("category_color", AdvanceSearchArrayList.get(position).category_color);
                                            bundle.putInt("color", AdvanceSearchArrayList.get(position).value);

                                            Intent intent = new Intent(TermsSearchActivity.this, BranchDetailsActivity.class);
                                            final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(TermsSearchActivity.this, false,
                                                    new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                                            ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(TermsSearchActivity.this, pairs);
                                            intent.putExtras(bundle);
                                            startActivity(intent, transitionActivityOptions.toBundle());
                                        }
                                    });
                                }

                            } else {
                                String responseMessage = obj_json.getString("Message");
                                termsListView.setVisibility(View.GONE);
                                termsErrorMessageTextView.setVisibility(View.VISIBLE);
                                termsErrorMessageTextView.setText(responseMessage);
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(TermsSearchActivity.this, "Bad server response.");
                            //AlertDialog("Server Error! Please try again.");
                        }
                    }
                });
    }
}
