package com.officelocator.activity;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import carbon.widget.TextView;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class ContactUsActivity extends AppCompatActivity implements View.OnClickListener/*,LoaderManager.LoaderCallbacks<Cursor> */ {

    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

    private EditText emailEditText;
    private EditText subjectEditText;
    private EditText commentEditText;
    private ProgressDialog mProgressDialog;

    // Global variable declaration
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    // for email validation
    public Pattern pattern;
    public Matcher matcher;

    private String email;
    private String subject;
    private String comment;

    public ContactUsActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.contact_us_activity);
        initView();
    }

    private void initView() {
        ((TextView) findViewById(R.id.txt_c_createOneTextView)).setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);

        mProgressDialog = new ProgressDialog(ContactUsActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        emailEditText = (EditText) findViewById(R.id.emailEditText);
        emailEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));
        subjectEditText = (EditText) findViewById(R.id.subjectEditText);
        subjectEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));
        commentEditText = (EditText) findViewById(R.id.commentEditText);
        commentEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));

        findViewById(R.id.txt_c_submit).setOnClickListener(this);

        if (!StorePreferences.getInstance().getEmailId().equals("")) {
            emailEditText.setText(StorePreferences.getInstance().getEmailId());
            emailEditText.setSelection(emailEditText.length());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                if (localValidation()) {
                    if (isValidEmail()) {
                        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                            SendContactUsAPICall();
                        } else {
                            ConnectionDetector.getInstance().show_alert(ContactUsActivity.this);
                        }
                    }
                }
                break;
        }
    }

    private void SendContactUsAPICall() {
        mProgressDialog.show();
        SingletonRestClient.get().sendContactUs(StorePreferences.getInstance().get_UseId(), email, subject, comment, new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response,
                                Response response2) {
                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("response");
                    if (responseStatus.equals("1")) {
                        emailEditText.setText("");
                        subjectEditText.setText("");
                        commentEditText.setText("");
                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(ContactUsActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                        onBackPressed();
                    } else {
                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(ContactUsActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    AppDelegate.LogE(e);
                    AppDelegate.showToast(ContactUsActivity.this, "Bad server response.");
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });

    }

    /**
     * Local validation check for the isEmpty
     *
     * @return true/false according to the validations
     */
    private Boolean localValidation() {

        email = emailEditText.getText().toString().trim();
        subject = subjectEditText.getText().toString().trim();
        comment = commentEditText.getText().toString().trim();
        Boolean response = false;

        if (email.equals("")) {
            emailEditText.setError(getResources().getString(R.string.contact_email_local_validation_message));
            emailEditText.requestFocus();
            response = false;
        } else if (subject.equals("")) {
            emailEditText.setError(null);
            subjectEditText.setError(getResources().getString(R.string.contact_subject_local_validation_message));
            subjectEditText.requestFocus();
            response = false;
        } else if (comment.equals("")) {
            emailEditText.setError(null);
            subjectEditText.setError(null);
            commentEditText.setError(getResources().getString(R.string.contact_comment_validation_message));
            commentEditText.requestFocus();
            response = false;
        } else {

            response = true;
        }
        return response;
    }

    /**
     * To check that email address is valid or not
     *
     * @return true/false according to the validations
     */
    private boolean isValidEmail() {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);

        if (!email.equals("")) {
            if (matcher.matches()) {
                emailEditText.setError(null);
                return true;
            } else {
                emailEditText.setError(getResources().getString(R.string.contact_email_local_validation_message));
                emailEditText.requestFocus();
                return false;
            }
        } else {
            return true;
        }
    }
}
