package com.officelocator.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.adapters.LanguageListAdapter;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.StorePreferences;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class NewSettingActivity extends AppCompatActivity implements View.OnClickListener
{
    ImageView img_c_sound, img_c_notification, img_c_vibration, img_c_range_5, img_c_range_10, img_c_range_20;
    Spinner spin_c_language;
    TextView txt_c_createOneTextView;

    public static final int LABEL_5 = 1, LABEL_10 = 2, LABEL_20 = 3;
    private String push_notification = "0";
    private String sound = "0";
    private String vibration = "0";
    private String lang = "es";
    private String latitude;
    private String longitude;
    private String radius;
    private Handler mHandler;

    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private ArrayList<String> languageList = new ArrayList<>();

    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        AppDelegate.set_locale(this, StorePreferences.getInstance().getLanguage());
        setContentView(R.layout.new_settings);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        // Initialization
        initView();
        setHandler();
    }

    private void initView()
    {
        txt_c_createOneTextView = (TextView) findViewById(R.id.txt_c_createOneTextView);
        txt_c_createOneTextView.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);

        img_c_sound = (ImageView) findViewById(R.id.img_c_sound);
        img_c_notification = (ImageView) findViewById(R.id.img_c_notification);
        img_c_vibration = (ImageView) findViewById(R.id.img_c_vibration);
        spin_c_language = (Spinner) findViewById(R.id.spin_c_language);
        img_c_range_5 = (ImageView) findViewById(R.id.img_c_range_5);
        img_c_range_5.setOnClickListener(this);
        img_c_range_10 = (ImageView) findViewById(R.id.img_c_range_10);
        img_c_range_10.setOnClickListener(this);
        img_c_range_20 = (ImageView) findViewById(R.id.img_c_range_20);
        img_c_range_20.setOnClickListener(this);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        findViewById(R.id.txt_c_submit).setOnClickListener(this);

        if (StorePreferences.getInstance().getNotificationSound())
        {
            sound = "1";
            img_c_sound.setSelected(true);
        }
        if (StorePreferences.getInstance().getPushNotificationShow())
        {
            push_notification = "1";
            img_c_notification.setSelected(true);
        }
        if (StorePreferences.getInstance().getIsVibration())
        {
            vibration = "1";
            img_c_vibration.setSelected(true);
        }
        lang = StorePreferences.getInstance().getLanguage();

        if (StorePreferences.getInstance().getRadius().equals(""))
        {
            setRange(LABEL_5);
        }
        else
        {
            if (StorePreferences.getInstance().getRadius().equals("5"))
            {
                setRange(LABEL_5);
            }
            else if (StorePreferences.getInstance().getRadius().equals("10"))
            {
                setRange(LABEL_10);
            }
            else if (StorePreferences.getInstance().getRadius().equals("20"))
            {
                setRange(LABEL_20);
            }
            else
            {
                setRange(LABEL_5);
            }
        }
        img_c_sound.setOnClickListener(this);
        img_c_notification.setOnClickListener(this);
        img_c_vibration.setOnClickListener(this);
        languageList.add(getString(R.string.english_radio_button_text));
        languageList.add(getString(R.string.spanish_radio_button_text));
        languageList.add(getString(R.string.maxico_radio_button_text));
        LanguageListAdapter countryListAdapter = new LanguageListAdapter(this, languageList);
        spin_c_language.setAdapter(countryListAdapter);
        spin_c_language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                switch (position)
                {
                    case 0:
                        lang = "en";
                        break;
                    case 1:
                        lang = "es";
                        break;
                    case 2:
                        lang = "es-MX";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                AppDelegate.LogT("lang = " + lang + ", " + StorePreferences.getInstance().getLanguage());
                switch (StorePreferences.getInstance().getLanguage())
                {
                    case "en":
                        spin_c_language.setSelection(0);
                        break;
                    case "es":
                        spin_c_language.setSelection(1);
                        break;
                    case "es-MX":
                        spin_c_language.setSelection(2);
                        break;
                }
//                if (lang.equalsIgnoreCase("en")) {
//                    spin_c_language.setSelection(0);
//                } else if (lang.equalsIgnoreCase("es")) {
//                    spin_c_language.setSelection(1);
//                } else if (lang.equalsIgnoreCase("es-MX")) {
//                    spin_c_language.setSelection(2);
//                }
            }
        }, 500);

       /*
       if (StorePreferences.getInstance().getLanguage().equals("")) {
            spanishRadioButton.setChecked(true);
        } else {
            if (StorePreferences.getInstance().getLanguage().equals("en")) {
                englishRadioButton.setChecked(true);
            } else if (StorePreferences.getInstance().getLanguage().equals("es-MX")) {
                maxicoRadioButton.setChecked(true);
            } else {
                spanishRadioButton.setChecked(true);
            }
        }*/

      /*  languageRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.englishRadioButton) {
                    lang = "en";
                    StorePreferences.getInstance().setLanguage(lang);
                } else if (checkedId == R.id.spanishRadioButton) {
                    lang = "es";
                    StorePreferences.getInstance().setLanguage(lang);
                } else if (checkedId == R.id.maxicoRadioButton) {
                    lang = "es-MX";
                    StorePreferences.getInstance().setLanguage(lang);
                }
            }
        });*/

//        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
////            getSettingsApiCall();
//        } else {
//            ConnectionDetector.getInstance().show_alert(this);
//        }
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                switch (msg.what)
                {
                    case 10:
                        AppDelegate.showProgressDialog(NewSettingActivity.this);
                        break;

                    case 11:
                        AppDelegate.hideProgressDialog(NewSettingActivity.this);
                        break;
                }
            }
        };
    }

    private void buildAlertMessageGps()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.gps_disable_alert_message))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.gps_disable_alert_yes_button_title), new DialogInterface.OnClickListener()
                {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                    {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.gps_disable_alert_no_button_title), new DialogInterface.OnClickListener()
                {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id)
                    {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void applySettingApiCall()
    {
        mProgressDialog.show();
        SingletonRestClient.get().sendSettings(StorePreferences.getInstance().get_UseId(), push_notification, sound, vibration, lang, StorePreferences.getInstance().getLatitude(),
                StorePreferences.getInstance().getLongitude(), radius, new Callback<Response>()
                {
                    @Override
                    public void failure(RestError restError)
                    {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void success(retrofit.client.Response response, retrofit.client.Response response2)
                    {
                        mProgressDialog.dismiss();
                        try
                        {
                            JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                            String responseStatus = obj_json.getString("response");
                            AppDelegate.LogT("responseStatus => " + obj_json);
                            if (responseStatus.equals("1"))
                            {
                                StorePreferences.getInstance().setLanguage(lang);
                                StorePreferences.getInstance().setRadius(radius);
                                StorePreferences.getInstance().setNotificationSound(sound.equalsIgnoreCase("1"));
                                StorePreferences.getInstance().setPushNotificationShow(push_notification.equalsIgnoreCase("1"));
                                StorePreferences.getInstance().setIsVibration(vibration.equalsIgnoreCase("1"));

                                String responseMessage = obj_json.getString("Message");
                                Toast.makeText(NewSettingActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(NewSettingActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(NewSettingActivity.this, false,
                                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                                        new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(NewSettingActivity.this, pairs);
                                startActivity(intent, transitionActivityOptions.toBundle());
                                finish();
                            }
                            else
                            {
                                String responseMessage = obj_json.getString("Message");
                                Toast.makeText(NewSettingActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception e)
                        {
                            // TODO Auto-generated catch block
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(NewSettingActivity.this, "Bad server response.");
                        }
                    }
                });
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txt_c_submit:
                applySettingApiCall();
                break;

            case R.id.img_c_sound:
                if (img_c_sound.isSelected())
                {
                    img_c_sound.setSelected(false);
                    sound = "0";
                    StorePreferences.getInstance().setNotificationSound(false);
                }
                else
                {
                    img_c_sound.setSelected(true);
                    sound = "1";
                    StorePreferences.getInstance().setNotificationSound(true);
                }
                break;
            case R.id.img_c_notification:
                if (img_c_notification.isSelected())
                {
                    img_c_notification.setSelected(false);
                    push_notification = "0";
                    StorePreferences.getInstance().setPushNotificationShow(false);
                }
                else
                {
                    img_c_notification.setSelected(true);
                    push_notification = "1";
                    StorePreferences.getInstance().setPushNotificationShow(true);
                }
                break;
            case R.id.img_c_vibration:
                if (img_c_vibration.isSelected())
                {
                    img_c_vibration.setSelected(false);
                    vibration = "0";
                    StorePreferences.getInstance().setIsVibration(false);
                }
                else
                {
                    img_c_vibration.setSelected(true);
                    vibration = "1";
                    StorePreferences.getInstance().setIsVibration(true);
                }
                break;
            case R.id.img_c_range_5:
                setRange(LABEL_5);
                break;
            case R.id.img_c_range_10:
                setRange(LABEL_10);
                break;
            case R.id.img_c_range_20:
                setRange(LABEL_20);
                break;
        }
    }

    void setRange(int value)
    {
        AppDelegate.LogT("value = " + value);
        img_c_range_5.setSelected(false);
        img_c_range_10.setSelected(false);
        img_c_range_20.setSelected(false);
        switch (value)
        {
            case LABEL_5:
                radius = "5";
                img_c_range_5.setSelected(true);
                break;
            case LABEL_10:
                radius = "10";
                img_c_range_10.setSelected(true);
                break;
            case LABEL_20:
                radius = "20";
                img_c_range_20.setSelected(true);
                break;
        }
    }
    //get android device Mac address
}
