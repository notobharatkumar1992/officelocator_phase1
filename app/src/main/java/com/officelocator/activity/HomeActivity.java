package com.officelocator.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.adapters.MapBrandListAdapter;
import com.officelocator.fragments.CustomMapFragment;
import com.officelocator.model.BranchList;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by himanshu on 3/24/2016.
 */
public class HomeActivity extends AppCompatActivity implements View.OnClickListener
{
    private int int_color = 0;
    private static Double latitude = 21.170240, longitude = 72.831062;
    private static String categoryId, category_color;

    private ListView BranchListView;
    private FrameLayout map_container;
    private ProgressDialog mProgressDialog;

    private TextView txt_c_createOneTextView, favoriteErrorMessageTextView;

    //ArrayList declaration
    ArrayList<BranchList.list> BranchArrayList;
    MapBrandListAdapter mapBrandListAdapter;

    public HomeActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.new_favourite_activity);

        categoryId = getIntent().getExtras().getString("categoryId");
        category_color = getIntent().getExtras().getString("category_color");
        int_color = getIntent().getExtras().getInt("color");

        Log.i("categoryId", categoryId + " + " + category_color);
        initView();
    }

    private void initView() {
        // Initialization
        txt_c_createOneTextView = (TextView) findViewById(R.id.txt_c_createOneTextView);
        txt_c_createOneTextView.setText(getResources().getString(R.string.title_nearby_branches));
        txt_c_createOneTextView.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);

        favoriteErrorMessageTextView = (TextView) findViewById(R.id.favoriteErrorMessageTextView);

        findViewById(R.id.txt_c_list).setOnClickListener(this);
        findViewById(R.id.txt_c_map).setOnClickListener(this);


        mProgressDialog = new ProgressDialog(HomeActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);
        BranchArrayList = new ArrayList<BranchList.list>();

        BranchListView = (ListView) findViewById(R.id.favoriteListView);
        map_container = (FrameLayout) findViewById(R.id.map_container);

        findViewById(R.id.txt_c_list).performClick();
    }

    public boolean mapLoadedOnce = false;

    public void loadMap() {
        try {
            if (!mapLoadedOnce) {
                mapLoadedOnce = true;
                Bundle bundle = new Bundle();
                bundle.putString("categoryId", categoryId);
                bundle.putString("category_color", category_color);
                bundle.putInt("color", int_color);
                bundle.putParcelableArrayList("value", BranchArrayList);
                CustomMapFragment mapFragment = new CustomMapFragment();
                mapFragment.setArguments(bundle);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.map_container, mapFragment).commit();
            } else {
                AppDelegate.LogT("HomeActivity, method loadMap, map loaded before");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txt_c_map) {

            loadMap();

            map_container.setVisibility(View.VISIBLE);
            BranchListView.setVisibility(View.GONE);

//            mapDisplayLinearLayout.setBackgroundColor(HomeActivity.this.getResources().getColor(R.color.colorPrimary));
//            listDisplayLinearLayout.setBackgroundColor(HomeActivity.this.getResources().getColor(R.color.white));
//
//            mapLabelTextView.setTextColor(HomeActivity.this.getResources().getColor(R.color.white));
//            listLabelTextView.setTextColor(HomeActivity.this.getResources().getColor(R.color.colorPrimary));

        } else if (v.getId() == R.id.txt_c_list) {

            BranchListView.setVisibility(View.VISIBLE);
            map_container.setVisibility(View.GONE);

//            mapDisplayLinearLayout.setBackgroundColor(HomeActivity.this.getResources().getColor(R.color.white));
//            listDisplayLinearLayout.setBackgroundColor(HomeActivity.this.getResources().getColor(R.color.colorPrimary));
//
//            mapLabelTextView.setTextColor(HomeActivity.this.getResources().getColor(R.color.colorPrimary));
//            listLabelTextView.setTextColor(HomeActivity.this.getResources().getColor(R.color.white));

            if (BranchArrayList.size() > 0) {

            } else {
                if (ConnectionDetector.getInstance().isConnectingToInternet())
                {
                    getBranchByCategoryAPiCall();
                } else {
                    ConnectionDetector.getInstance().show_alert(HomeActivity.this);
                }
            }
        }
    }

    private void getBranchByCategoryAPiCall() {
        mProgressDialog.show();//"26.849088", "75.804443"
        String radius = "";
        if (!StorePreferences.getInstance().getRadius().equals("")) {
            radius = StorePreferences.getInstance().getRadius();
        }
        else
        {
            radius = "5";
        }

        try
        {
            latitude = Double.parseDouble(StorePreferences.getInstance().getLatitude());
            longitude = Double.parseDouble(StorePreferences.getInstance().getLongitude());
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }

        latitude = 19.36889400;
        longitude = -99.1803240000000000;
        StorePreferences.getInstance().setLatitude(latitude + "");
        StorePreferences.getInstance().setLongitude(longitude + "");

        SingletonRestClient.get().getBranchByCategory(categoryId, latitude + "", longitude + "", radius, StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response,
                                Response response2) {

                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {
                        JSONArray list = new JSONArray(obj_json.getString("list"));

                        if (list.length() > 0) {
                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            BranchList mBranchList = gson.fromJson(String.valueOf(
                                    new String(((TypedByteArray) response.getBody()).getBytes())), BranchList.class);

                            BranchArrayList = mBranchList.list;
//                            int count = 0;
                            for (int i = 0; i < BranchArrayList.size(); i++) {
                                BranchArrayList.get(i).value = int_color;
                                BranchArrayList.get(i).category_color = category_color;
//                                if (count == AppDelegate.TILES_COLOR_COUNT) {
//                                    count = 0;
//                                } else {
//                                    count++;
//                                }
                            }

                            Log.i("BranchArrayList", BranchArrayList.size() + "==");
                            mapBrandListAdapter = new MapBrandListAdapter(HomeActivity.this, BranchArrayList);
                            BranchListView.setAdapter(mapBrandListAdapter);

                            BranchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("Id", BranchArrayList.get(position).id);
                                    bundle.putString("BranchId", BranchArrayList.get(position).category_id);
                                    bundle.putString("category_color", BranchArrayList.get(position).category_color);
                                    bundle.putInt("color", BranchArrayList.get(position).value);

                                    Intent intent = new Intent(HomeActivity.this, BranchDetailsActivity.class);
                                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(HomeActivity.this, false,
                                            new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(HomeActivity.this, pairs);
                                    intent.putExtras(bundle);
                                    startActivity(intent, transitionActivityOptions.toBundle());
                                }
                            });

                            favoriteErrorMessageTextView.setVisibility(View.GONE);
                            loadMap();
                        }
                    } else {
                        String responseMessage = obj_json.getString("Message");
                        favoriteErrorMessageTextView.setVisibility(View.VISIBLE);
                        Toast.makeText(HomeActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    AppDelegate.LogE(e);
                    AppDelegate.showToast(HomeActivity.this, "Bad server response.");
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });
    }
}
