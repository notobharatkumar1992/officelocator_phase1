package com.officelocator.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.officelocator.AppDelegate;
import com.officelocator.Constants.Tags;
import com.officelocator.Interfaces.OnListItemClickListener;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.adapters.LocationMapAdapter;
import com.officelocator.adapters.SearchCategoryListAdapter;
import com.officelocator.async.LocationAddress;
import com.officelocator.async.PlacesService;
import com.officelocator.expandablelayout.ExpandableRelativeLayout;
import com.officelocator.model.Place;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import carbon.widget.ProgressBar;
import carbon.widget.TextView;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener, OnListItemClickListener
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //Control declaration
    private EditText postalCodeEditText;
    private EditText productNameEditText;
    private Spinner categorySpinner;
    private ProgressDialog mProgressDialog;

    //ArrayList declaration
    ArrayList<String> categoryArrayList;
    ArrayList<String> categoryIdArrayList;
    ArrayList<String> categoryColorArrayList;

    //Global variable declaration
    private String categoryId;
    private String category_color;
    private String postalCode = "";
    private String productName = "";

    private OnFragmentInteractionListener mListener;
    public Activity mActivity;

    public Handler mHandler;
    public List<Place> mapPlacesArray_pickup = new ArrayList<>();
    public Thread mThreadOnSearch_pickup;
    public LocationMapAdapter adapter_pickup;
    public ListView search_list_pickup;
    public TextView no_location_text_pickup;
    public ProgressBar pb_c_location_progressbar_pickup;
    public Place placeDetail_pickup;
    public ExpandableRelativeLayout exp_layout;
    public static final int COLLAPSE = 0, EXPAND = 1;
    public double currentLatitude, currentLongitude;

    public ScrollView scrollView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.new_search_activity);


        mActivity = this;
        canSearch = true;
        initView();
        setHandler();
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                switch (msg.what)
                {
                    case 10:
                        AppDelegate.showProgressDialog(SearchActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SearchActivity.this);
                        break;
                    case 2:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        mHandler.sendEmptyMessage(10);
                        AppDelegate.LogT("mHandler.sendEmptyMessage(3) called choose location Activity;");
                        LocationAddress.getLocationFromReverseGeocoder(postalCodeEditText.getText().toString(), SearchActivity.this, mHandler);
                        break;
                    case 4:
                        AppDelegate.LogT("Handler 4 called");
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        if (adapter_pickup != null && search_list_pickup != null)
                        {
                            adapter_pickup.notifyDataSetChanged();
                            search_list_pickup.invalidate();
                        }
                        if (mapPlacesArray_pickup != null && mapPlacesArray_pickup.size() > 0)
                        {
                            if (postalCodeEditText.length() > 0)
                            {
                                AppDelegate.LogT("handler 1");
                                search_list_pickup.setVisibility(View.VISIBLE);
                                no_location_text_pickup.setVisibility(View.GONE);
                            }
                            else
                            {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        else
                        {
                            search_list_pickup.setVisibility(View.GONE);
                            if (AppDelegate.zeroResult)
                            {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.no_location_available_for_this_address));
                            }
                            else
                            {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text_pickup.setVisibility(View.VISIBLE);
                            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 5:
                        AppDelegate.LogT("Handler 5 called");
                        currentLatitude = 0;
                        currentLongitude = 0;
                        if (canSearch)
                        {
                            scrollView.post(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    scrollView.smoothScrollTo(0, scrollView.getBottom());
                                }
                            });

                            if (postalCodeEditText.length() != 0)
                            {
                                pb_c_location_progressbar_pickup.setVisibility(View.VISIBLE);
                                stopStartThread(mThreadOnSearch_pickup);
                                no_location_text_pickup.setVisibility(View.GONE);
                                expandableView(exp_layout, EXPAND);
                            }
                            else
                            {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        else
                        {
                            if (postalCodeEditText.length() != 0)
                            {
                            }
                            else
                            {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        break;
                    case 9:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 21:
                        no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        no_location_text_pickup.setVisibility(View.VISIBLE);
                        search_list_pickup.setVisibility(View.GONE);
                        expandableView(exp_layout, EXPAND);
                        break;
                    case 22:
                        if (postalCodeEditText.length() != 0)
                        {
                        }
                        else
                        {
                            expandableView(exp_layout, COLLAPSE);
                        }
                        break;

                }
            }
        };
    }

    public void expandableView(final ExpandableRelativeLayout expLayout, final int value)
    {
        expLayout.post(new Runnable()
        {
            @Override
            public void run()
            {
                if (value == COLLAPSE)
                {
                    expLayout.collapse();
                }
                else
                {
                    expLayout.expand();
                }
            }
        });
    }

    private void getLatLngFromGEOcoder(Bundle data)
    {
        mHandler.sendEmptyMessage(11);
        AppDelegate.LogT("for latlng => " + data.getString(Tags.forLatlng) + ", latitude = " + currentLatitude + ", longitude = " + currentLongitude);
        if (AppDelegate.isValidString(data.getString(Tags.forLatlng)) && data.getString(Tags.forLatlng).equalsIgnoreCase(Tags.forLatlng))
        {
            try
            {
                currentLatitude = data.getDouble(Tags.latitude);
                currentLongitude = data.getDouble(Tags.longitude);
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
            }
        }
        else
        {
        }
    }


    private void stopStartThread(Thread theThread)
    {
        stopTimer();
        onlyStopThread(mThreadOnSearch_pickup);
        if (!AppDelegate.haveNetworkConnection(SearchActivity.this, false))
        {
            no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
            no_location_text_pickup.setVisibility(View.VISIBLE);
            search_list_pickup.setVisibility(View.GONE);
            expandableView(exp_layout, EXPAND);
        }
        else
        {
            expandableView(exp_layout, EXPAND);
            mTimer = new Timer();
            mtimerTask = new TimerTask()
            {
                public void run()
                {
                    mThreadOnSearch_pickup = new Thread(mTimerRunnable);
                    mThreadOnSearch_pickup.start();
                }
            };
            mTimer.schedule(mtimerTask, 1000);
        }
    }

    private void onlyStopThread(Thread mThreadOnSearch)
    {
        if (mThreadOnSearch != null)
        {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    private void stopTimer()
    {
        if (mTimer != null)
        {
            mTimer.cancel();
            mTimer.purge();
        }
        if (mTimerHandler != null && mTimerRunnable != null)
        {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
    }

    private boolean canSearch = false;
    private Timer mTimer;
    private TimerTask mtimerTask;
    private Handler mTimerHandler = new Handler();
    private Runnable mTimerRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            if (!AppDelegate.haveNetworkConnection(SearchActivity.this, false))
            {
                mHandler.sendEmptyMessage(21);
            }
            else if (postalCodeEditText.length() == 0)
            {
                mHandler.sendEmptyMessage(22);
            }
            else
            {
                mapPlacesArray_pickup.clear();
                mapPlacesArray_pickup.addAll(PlacesService.autocompleteForMap(postalCodeEditText.getText().toString()));

                //AppDelegate.LogE("mapPlacesArray_pickup ======= " + mapPlacesArray_pickup.toString());

                mHandler.sendEmptyMessage(4);
                canSearch = true;
            }
        }
    };


    private void addtextWatcher()
    {
        postalCodeEditText.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                if (canSearch)
                {
                    scrollView.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scrollView.smoothScrollTo(0, scrollView.getBottom());
                        }
                    });
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                mHandler.sendEmptyMessage(5);
            }
        });
    }

    private void initView()
    {
        currentLatitude = 0;
        currentLongitude = 0;

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        mProgressDialog = new ProgressDialog(SearchActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
        postalCodeEditText = (EditText) findViewById(R.id.postalCodeEditText);
        productNameEditText = (EditText) findViewById(R.id.productNameEditText);
        findViewById(R.id.txt_c_search).setOnClickListener(this);

        // Get Category API call
        getCategoriesAPICall();

        addtextWatcher();
        no_location_text_pickup = (TextView) findViewById(R.id.no_location_text);
        search_list_pickup = (ListView) findViewById(R.id.search_list);
        adapter_pickup = new LocationMapAdapter(this, mapPlacesArray_pickup, this);
        search_list_pickup.setAdapter(adapter_pickup);
        pb_c_location_progressbar_pickup = (ProgressBar) findViewById(R.id.pb_c_location_progressbar);
        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        expandableView(exp_layout, COLLAPSE);

        AutocompleteFilter typeFilter1 = new AutocompleteFilter.Builder().
                setTypeFilter(com.google.android.gms.location.places.Place.TYPE_COUNTRY).setCountry("mx").build();
        final PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(15.0f);

        autocompleteFragment.setFilter(typeFilter1);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener()
        {
            @Override
            public void onPlaceSelected(com.google.android.gms.location.places.Place place)
            {
                currentLatitude = place.getLatLng().latitude;
                currentLongitude = place.getLatLng().longitude;

                ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setText(place.getAddress());
            }

            @Override
            public void onError(Status status)
            {

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri)
    {
        if (mListener != null)
        {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mListener = null;
        mActivity = null;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txt_c_search:
                AppDelegate.LogE(currentLatitude + "");
                AppDelegate.LogE(currentLongitude + "");

                postalCode = postalCodeEditText.getText().toString().trim();
                productName = productNameEditText.getText().toString().trim();
                if (postalCodeEditText.length() > 0)
                {
                    if (currentLatitude == 0 && currentLongitude == 0)
                    {
                        AppDelegate.showToast(SearchActivity.this, "Please select valid location.");
                    }
                    else
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString("categoryId", categoryId);
                        bundle.putString("category_color", category_color);
                        bundle.putString("postalCode", postalCode);
                        bundle.putDouble("lat", currentLatitude);
                        bundle.putDouble("lng", currentLongitude);
                        bundle.putString("productName", productName);
                        Intent intent = new Intent(SearchActivity.this, SearchedResultActivity.class);
                        intent.putExtras(bundle);
                        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SearchActivity.this, false,
                                new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                                new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SearchActivity.this, pairs);
                        SearchActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
                    }
                }
                else
                {
                    Bundle bundle = new Bundle();
                    bundle.putString("categoryId", categoryId);
                    bundle.putString("category_color", category_color);
                    bundle.putString("postalCode", postalCode);
                    bundle.putString("lat", currentLatitude + "");
                    bundle.putString("lng", currentLongitude + "");
                    bundle.putString("productName", productName);
                    Intent intent = new Intent(SearchActivity.this, SearchedResultActivity.class);
                    intent.putExtras(bundle);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SearchActivity.this, false,
                            new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                            new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SearchActivity.this, pairs);
                    SearchActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
                }
                break;
        }
    }

    /**
     * This Interfaces must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void getCategoriesAPICall()
    {
        if (ConnectionDetector.getInstance().isConnectingToInternet())
        {
            mProgressDialog.show();
            SingletonRestClient.get().getAllCategories(StorePreferences.getInstance().getLanguage(), new Callback<Response>()
            {
                @Override
                public void failure(RestError restError)
                {
                    mProgressDialog.dismiss();
                }

                @Override
                public void success(Response response, Response response2)
                {
                    if (mActivity == null)
                    {
                        return;
                    }
                    mProgressDialog.dismiss();
                    try
                    {
                        JSONObject obj_json = new JSONObject(
                                new String(((TypedByteArray) response.getBody()).getBytes()));
                        Boolean responseStatus = obj_json.getBoolean("Status");
                        if (responseStatus)
                        {
                            //String responseMessage = obj_json.getString("responseMessage");
                            JSONArray obJsonArray = obj_json.getJSONArray("list");

                            if (obJsonArray.length() > 0)
                            {
                                categoryArrayList = new ArrayList<>();
                                categoryIdArrayList = new ArrayList<>();
                                categoryColorArrayList = new ArrayList<>();

                                /*categoryArrayList.add("Choose Category");
                                categoryIdArrayList.add("Choose Category");
                                categoryColorArrayList.add("Choose Category");*/

                                //////////////////////
                                categoryArrayList.add(getString(R.string.choose_category));
                                categoryIdArrayList.add(getString(R.string.choose_category));
                                categoryColorArrayList.add(getString(R.string.choose_category));
                                //////////////////////

                                for (int i = 0; i < obJsonArray.length(); i++)
                                {
                                    JSONObject jsonObject = obJsonArray.getJSONObject(i);
                                    categoryArrayList.add(jsonObject.getString("name"));
                                    categoryIdArrayList.add(jsonObject.getString("id"));
                                    categoryColorArrayList.add(jsonObject.getString("category_color"));
                                }
                            }

                            // Creating adapter for spinner
                            SearchCategoryListAdapter categoryListAdapter = new SearchCategoryListAdapter(SearchActivity.this, categoryArrayList);
                            // attaching data adapter to spinner
                            categorySpinner.setAdapter(categoryListAdapter);

                            categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                            {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                                {
                                    if (position == 0)
                                    {
                                        categoryId = "";
                                        category_color = "";
                                    }
                                    else
                                    {
                                        categoryId = categoryIdArrayList.get(position);
                                        category_color = categoryColorArrayList.get(position);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent)
                                {

                                }
                            });

                        }
                        else
                        {

                        }
                    }
                    catch (Exception e)
                    {
                        // TODO Auto-generated catch block
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(SearchActivity.this, "Bad server response.");
                        //AlertDialog("Server Error! Please try again.");
                    }
                }
            });
        }
        else
        {
            ConnectionDetector.getInstance().show_alert(SearchActivity.this);
        }
    }

//    /**
//     * Local validation check for the isEmpty
//     *
//     * @return true/false according to the validations
//     */
//    private Boolean localValidation() {
//        postalCode = postalCodeEditText.getText().toString().trim();
//        productName = productNameEditText.getText().toString().trim();
//        Boolean response = true;
//        return response;
//    }

    @Override
    public void setOnListItemClickListener(String name, int position)
    {
        AppDelegate.hideKeyBoard(SearchActivity.this);
        if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(SearchActivity.this))
        {
            canSearch = false;
            if (mapPlacesArray_pickup.size() > position)
            {
                placeDetail_pickup = mapPlacesArray_pickup.get(position);
                if (AppDelegate.isValidString(placeDetail_pickup.name) && AppDelegate.isValidString(placeDetail_pickup.vicinity))
                {
                    postalCodeEditText.setText(placeDetail_pickup.name + ", " + placeDetail_pickup.vicinity);
                }
                else if (AppDelegate.isValidString(placeDetail_pickup.name))
                {
                    postalCodeEditText.setText(placeDetail_pickup.name);
                }
                else if (AppDelegate.isValidString(placeDetail_pickup.vicinity))
                {
                    postalCodeEditText.setText(placeDetail_pickup.vicinity);
                }
                postalCodeEditText.setSelection(postalCodeEditText.length());
                expandableView(exp_layout, COLLAPSE);
                mHandler.sendEmptyMessage(3);
            }
            canSearch = true;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP)
    {

    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean checked)
    {

    }
}