package com.officelocator.activity;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.codemybrainsout.ratingdialog.RatingDialog;
import com.officelocator.AppDelegate;
import com.officelocator.Constants.Tags;
import com.officelocator.R;
import com.officelocator.adapters.ReviewsListAdapter;
import com.officelocator.model.ReviewModel;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class RatingActivity extends AppCompatActivity implements View.OnClickListener
{
    private TextView termsErrorMessageTextView, txt_c_createOneTextView;
    private ImageView img_c_add;
    private ProgressDialog mProgressDialog;
    ArrayList<ReviewModel> arrayReviewModel = new ArrayList<>();
    public RecyclerView rv_category;
    public ReviewsListAdapter manageRequestAdapter;
    public String str_branchDetail_id = "";
    public int review_rating = 0;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        str_branchDetail_id = getIntent().getExtras().getString(Tags.id);
        AppDelegate.set_locale(this, StorePreferences.getInstance().getLanguage());
        setContentView(R.layout.new_reviews_activity);
        initView();
    }

    private void initView()
    {
        ((carbon.widget.TextView) findViewById(R.id.txt_c_createOneTextView)).setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);
        mProgressDialog = new ProgressDialog(RatingActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        rv_category = (RecyclerView) findViewById(R.id.rv_category);
        rv_category.setLayoutManager(new LinearLayoutManager(RatingActivity.this));
        rv_category.setHasFixedSize(true);
        rv_category.setItemAnimator(new DefaultItemAnimator());

        termsErrorMessageTextView = (TextView) findViewById(R.id.termsErrorMessageTextView);
        txt_c_createOneTextView = (TextView) findViewById(R.id.txt_c_createOneTextView);
        txt_c_createOneTextView.setText(getString(R.string.ratings));
        img_c_add = (ImageView) findViewById(R.id.img_c_add);
        img_c_add.setVisibility(View.VISIBLE);
        img_c_add.setOnClickListener(this);

        if (ConnectionDetector.isConnectingToInternet())
        {
            getBranchDetailApiCall();
        }
        else
        {
            ConnectionDetector.show_alert(RatingActivity.this);
        }
    }

    private void getBranchDetailApiCall()
    {
        mProgressDialog.show();
        SingletonRestClient.get().getBranchDetail(str_branchDetail_id, StorePreferences.getInstance().get_UseId(), StorePreferences.getInstance().getLanguage(), new Callback<Response>()
        {
            @Override
            public void failure(RestError restError)
            {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response, Response response2)
            {
                mProgressDialog.dismiss();
                try
                {
                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    arrayReviewModel.clear();
                    if (responseStatus.equals("true"))
                    {
                        JSONObject list = obj_json.getJSONObject("list");
                        JSONArray review = list.getJSONArray("review");
                        try
                        {
                            review_rating = list.optInt("review_rating");
                        }
                        catch (Exception e)
                        {
                            AppDelegate.LogE(e);
                        }
                        if (review.length() > 0)
                        {
                            Log.i("product", "=" + review.length());
                            for (int j = 0; j < review.length(); j++)
                            {
                                JSONObject jsonObject = review.getJSONObject(j);
                                ReviewModel reviewModel = new ReviewModel();
                                reviewModel.id = jsonObject.getString(Tags.id);
                                reviewModel.branch_id = jsonObject.getString(Tags.branch_id);
                                reviewModel.rating = jsonObject.getString(Tags.rating);
                                reviewModel.review = jsonObject.getString(Tags.review);
                                reviewModel.created = jsonObject.getString(Tags.created);

                                reviewModel.full_name = jsonObject.getJSONObject(Tags.user).getString(Tags.first_name) + " " + jsonObject.getJSONObject(Tags.user).getString(Tags.last_name);
                                arrayReviewModel.add(reviewModel);
                            }
                        }
                        if (review_rating == 0)
                        {
                            img_c_add.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            img_c_add.setVisibility(View.GONE);
                        }
                        if (arrayReviewModel.size() == 0)
                        {
                            AppDelegate.showToast(RatingActivity.this, "No review found.");
                        }
                        manageRequestAdapter = new ReviewsListAdapter(RatingActivity.this, arrayReviewModel);
                        rv_category.setAdapter(manageRequestAdapter);
                    }
                    else
                    {
                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(RatingActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception e)
                {
                    AppDelegate.LogE(e);
                    AppDelegate.showToast(RatingActivity.this, "Bad server response.");
                }
            }
        });
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_c_add:
//                AppDelegate.showAlert(RatingActivity.this, "Rate Branch", "Are you sure you want to rate this branch?", "Yes", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
                showRatingDialog();
//                    }
//                }, "No", null);
                break;
        }
    }

    public float selectedRating = 0;
    public String str_rateDialogFeedback = "";

    private void showRatingDialog()
    {
        final RatingDialog ratingDialog = new RatingDialog.Builder(this)
                .session(150)
                .threshold(5)
                .ratingBarColor(R.color.colorAccent)
                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener()
                {
                    @Override
                    public void onFormSubmitted(String feedback)
                    {
                        str_rateDialogFeedback = feedback;
                        executeAddRatingAsync();
                    }
                }).onRatingChanged(new RatingDialog.Builder.RatingDialogListener()
                {
                    @Override
                    public void onRatingSelected(float rating, boolean thresholdCleared)
                    {
                        selectedRating = rating;
                    }
                })
                .build();
        ratingDialog.show();
    }

    private void executeAddRatingAsync()
    {
        if (AppDelegate.haveNetworkConnection(this))
        {
            mProgressDialog.show();
            SingletonRestClient.get().addReview(StorePreferences.getInstance().get_UseId() + "",
                    str_branchDetail_id + "",
                    "Review",
                    str_rateDialogFeedback + "",
                    selectedRating + "",
                    new Callback<Response>()
                    {

                        @Override
                        public void success(Response response, Response response2)
                        {
                            mProgressDialog.dismiss();
                            try
                            {
                                getBranchDetailApiCall();
                            }
                            catch (Exception e)
                            {
                                AppDelegate.LogE(e);
                                AppDelegate.showToast(RatingActivity.this, "Bad server response.");
                            }
                        }

                        @Override
                        public void failure(RestError restError)
                        {
                            mProgressDialog.dismiss();
                        }

                        @Override
                        public void failure(RetrofitError error)
                        {
                            super.failure(error);
                            mProgressDialog.dismiss();
                        }
                    });
        }
    }

}
