package com.officelocator.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONObject;

import java.util.Locale;

import carbon.widget.TextView;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class SecurityQuestionValidationActivity extends AppCompatActivity
{
    public static Activity mActivity;
    private TextView securityQuestionTextView;
    private EditText securityAnswerEditText;
    private ProgressDialog mProgressDialog;

    // Global variable declaration
    private String id = "";
    private String question_id = "";
    private String question = "";
    private String Language = "";
    private String securityAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        set_locale(StorePreferences.getInstance().getTempLanguage());
        setContentView(R.layout.new_security_question_validation_activity);
        mActivity = this;

        ((carbon.widget.TextView) findViewById(R.id.txt_c_createOneTextView)).setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);

        // Initialization
        mProgressDialog = new ProgressDialog(SecurityQuestionValidationActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        id = getIntent().getStringExtra("id");
        question_id = getIntent().getStringExtra("question_id");
        question = getIntent().getStringExtra("question");
        Language = getIntent().getStringExtra("Language");

        securityQuestionTextView = (TextView) findViewById(R.id.securityQuestionTextView);
        securityAnswerEditText = (EditText) findViewById(R.id.securityAnswerEditText);
        securityAnswerEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));

        securityQuestionTextView.setText(Html.fromHtml("<b>" + getString(R.string.question_forget_password) + "</b> " + question + ""));

        findViewById(R.id.txt_c_submit).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                securityAnswer = securityAnswerEditText.getText().toString().trim();
                if (securityAnswer.equals(""))
                {
                    securityAnswerEditText.setError(getResources().getString(R.string.security_answer_local_validation_message));
                }
                else
                {
                    securityAnswerEditText.setError(null);
                    if (ConnectionDetector.getInstance().isConnectingToInternet())
                    {
                        checkUserSecurityAnswerAPICall();
                    }
                    else
                    {
                        ConnectionDetector.getInstance().show_alert(SecurityQuestionValidationActivity.this);
                    }
                }
            }
        });
    }

    private void checkUserSecurityAnswerAPICall()
    {
        mProgressDialog.show();
        SingletonRestClient.get().checkUserSecurityAnswer(id, question_id,
                securityAnswer, new Callback<Response>()
                {
                    @Override
                    public void failure(RestError restError)
                    {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void success(Response response,
                                        Response response2)
                    {
                        mProgressDialog.dismiss();
                        try
                        {
                            JSONObject obj_json = new JSONObject(
                                    new String(((TypedByteArray) response.getBody()).getBytes()));
                            Boolean responseStatus = obj_json.getBoolean("Status");
                            if (responseStatus)
                            {
                                JSONObject list = new JSONObject(obj_json.getString("list"));
                                String user_id = list.getString("user_id");
                                //StorePreferences.getInstance().set_UserId(user_id);
                                Intent mainActivityIntent = new Intent(SecurityQuestionValidationActivity.this, ChangePasswordActivity.class);
                                mainActivityIntent.putExtra("user_id", user_id);
                                mainActivityIntent.putExtra("Language", Language);
                                startActivity(mainActivityIntent);

                                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SecurityQuestionValidationActivity.this, false,
                                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                                        new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SecurityQuestionValidationActivity.this, pairs);
                                startActivity(mainActivityIntent, transitionActivityOptions.toBundle());

                            }
                            else
                            {
                                String responseMessage = obj_json.getString("Message");
                                Toast.makeText(SecurityQuestionValidationActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception e)
                        {
                            // TODO Auto-generated catch block
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(SecurityQuestionValidationActivity.this, "Bad server response.");
                            //AlertDialog("Server Error! Please try again.");
                        }
                    }
                });
    }

    private void set_locale(String lan)
    {
        Log.d("test", "set_locale => " + lan);
        Locale locale = new Locale(lan);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources()
                .updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mActivity = null;
    }

    public static boolean finishActivity()
    {
        try
        {
            if (mActivity != null)
            {
                mActivity.finish();
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            return false;
        }
    }
}
