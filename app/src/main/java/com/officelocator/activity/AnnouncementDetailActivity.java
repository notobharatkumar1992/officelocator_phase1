package com.officelocator.activity;

import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.officelocator.AppDelegate;
import com.officelocator.Constants.Tags;
import com.officelocator.R;
import com.officelocator.model.AnnouncementListData;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.StorePreferences;

import org.json.JSONObject;

import java.util.Locale;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by himanshu on 3/24/2016.
 */
public class AnnouncementDetailActivity extends AppCompatActivity {

    private TextView announcementTitleTextView;
    private TextView announcementCommentTextView;
    private ProgressDialog mProgressDialog;

    //Global variable declaration
    private String announcementId = "";
    private AnnouncementListData.list sliderModel;

    public ImageView img_large, img_loading;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        }
        set_locale(StorePreferences.getInstance().getLanguage());
        setContentView(R.layout.new_announcement_detail_activity);
        initView();
        setValues();
    }

    private void setValues() {
        ((carbon.widget.TextView) findViewById(R.id.txt_c_createOneTextView)).setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);
        announcementTitleTextView.setText(sliderModel.name);
        announcementCommentTextView.setText(sliderModel.comment);

        img_loading.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
        frameAnimation.setCallback(img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        imageLoader.loadImage(sliderModel.logo, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                img_large.setImageDrawable(getResources().getDrawable(R.drawable.sample_bg));
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                img_large.setImageBitmap(bitmap);
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                img_large.setImageDrawable(getResources().getDrawable(R.drawable.sample_bg));
                img_loading.setVisibility(View.GONE);
            }
        });
    }

    private void initView() {
        announcementId = getIntent().getExtras().getString("announcementId");
        sliderModel = getIntent().getExtras().getParcelable(Tags.slider_id);


        img_large = (ImageView) findViewById(R.id.img_large);
        img_loading = (ImageView) findViewById(R.id.img_loading);

        // Initialization
        mProgressDialog = new ProgressDialog(AnnouncementDetailActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        announcementTitleTextView = (TextView) findViewById(R.id.announcementTitleTextView);
        announcementCommentTextView = (TextView) findViewById(R.id.announcementCommentTextView);

//        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
//            getAnnouncementDetailsApiCall();
//        } else {
//            ConnectionDetector.getInstance().show_alert(AnnouncementDetailActivity.this);
//        }
    }

    private void getAnnouncementDetailsApiCall() {
        mProgressDialog.show();
        SingletonRestClient.get().getAnnouncementDetail(announcementId, StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response,
                                Response response2) {
                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {

                        JSONObject output = obj_json.getJSONObject("output");
                        if (output.getString("name") != null) {
                            announcementTitleTextView.setText(output.getString("name"));
                        }
                        if (output.getString("comment") != null) {
                            announcementCommentTextView.setText(output.getString("comment"));
                        }

                    } else {

                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                    AppDelegate.showToast(AnnouncementDetailActivity.this, "Bad server response.");
                }
            }
        });
    }

    private void set_locale(String lan) {
        Log.d("test", "set_locale => " + lan);
        Locale locale = new Locale(lan);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

}
