package com.officelocator.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by sgermain on 5/17/15.
 */
public class RoundedImageView extends ImageView {

	private int borderWidth;
	private int viewWidth, viewHeight;

	private Paint paint;
	private Paint paintBorder;

	public RoundedImageView(Context context) {

		super(context);
		setup();
	}

	public RoundedImageView(Context context, AttributeSet attrs) {

		super(context, attrs);
		setup();
	}

	public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {

		super(context, attrs, defStyle);
		setup();
	}

	private void setup() {

		paint = new Paint();
		paint.setAntiAlias(true);

		paintBorder = new Paint();
		setBorderColor(Color.WHITE);
		setBorderWidth(2);
		paintBorder.setAntiAlias(true);
	}

	private void setBorderWidth(int borderWidth) {

		this.borderWidth = borderWidth;
		invalidate();
	}

	private void setBorderColor(int borderColor) {

		if (paintBorder != null) {
			paintBorder.setColor(borderColor);
		}

		invalidate();
	}

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(@NonNull Canvas canvas) {

		BitmapDrawable bitmapDrawable = (BitmapDrawable)getDrawable();
		Bitmap image = null;

		if (bitmapDrawable != null) {
			image = bitmapDrawable.getBitmap();
		}

		if (image != null) {
			BitmapShader shader = new BitmapShader(
					Bitmap.createScaledBitmap(image, canvas.getWidth(), canvas.getHeight(), false), Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
			paint.setShader(shader);
			int circleCenter = viewWidth / 2;

			canvas.drawCircle(circleCenter + borderWidth, circleCenter + borderWidth, circleCenter + borderWidth, paintBorder);
			canvas.drawCircle(circleCenter + borderWidth, circleCenter + borderWidth, circleCenter, paint);
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		int width;
		int height;

		int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);

		int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);

		if (widthSpecMode == MeasureSpec.EXACTLY) {
			width = widthSpecSize;
		}
		else {
			width = viewWidth;
		}

		if (heightSpecMode == MeasureSpec.EXACTLY) {
			height = heightSpecSize;
		}
		else {
			height = viewHeight;
		}

		viewWidth = width - (borderWidth * 2);
		viewHeight = height - (borderWidth * 2);

		setMeasuredDimension(width, height);
	}
}
