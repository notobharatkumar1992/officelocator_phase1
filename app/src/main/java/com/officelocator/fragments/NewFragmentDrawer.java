package com.officelocator.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.officelocator.R;
import com.officelocator.util.CircleImageView;
import com.officelocator.util.StorePreferences;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by himanshu on 3/24/2016.
 */
public class NewFragmentDrawer extends Fragment implements View.OnClickListener
{
    private static String TAG = NewFragmentDrawer.class.getSimpleName();

    public Context mContext;
    public ImageView img_c_loading;
    public CircleImageView cimg_user;
    public android.widget.TextView txt_c_name;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View containerView;
    private static String[] titles = null;
    private FragmentDrawerListener drawerListener;

    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();


    public NewFragmentDrawer()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // drawer labels
        titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.new_drawer, container, false);
        return layout;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        mContext = getActivity();
        initView(view);
        updateData();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mContext = null;
    }

    private void initView(View view)
    {
        ((TextView) view.findViewById(R.id.txt_c_menu)).setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);
        view.findViewById(R.id.rl_c_home).setOnClickListener(this);
        view.findViewById(R.id.ll_my_profile).setOnClickListener(this);
        view.findViewById(R.id.rl_c_my_profile).setOnClickListener(this);
        view.findViewById(R.id.rl_c_my_fav).setOnClickListener(this);
        view.findViewById(R.id.rl_c_search).setOnClickListener(this);
        view.findViewById(R.id.rl_c_setting).setOnClickListener(this);
        view.findViewById(R.id.rl_c_contact_us).setOnClickListener(this);
        view.findViewById(R.id.rl_c_announcements).setOnClickListener(this);
        view.findViewById(R.id.rl_c_privacy_policy).setOnClickListener(this);
        view.findViewById(R.id.rl_c_logout).setOnClickListener(this);

        txt_c_name = (android.widget.TextView) view.findViewById(R.id.txt_c_name);
        txt_c_name.setSelected(true);
        txt_c_name.setText(StorePreferences.getInstance().get_Name());
        cimg_user = (CircleImageView) view.findViewById(R.id.cimg_user);
        img_c_loading = (ImageView) view.findViewById(R.id.img_c_loading);


        //        img_c_loading.setVisibility(View.VISIBLE);
//        AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading.getDrawable();
//        frameAnimation.setCallback(img_c_loading);
//        frameAnimation.setVisible(true, true);
//        frameAnimation.start();

        try
        {
            if (!StorePreferences.getInstance().getUserProfilePhoto().equals(""))
            {
                img_c_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading.getDrawable();
                frameAnimation.setCallback(img_c_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();

                imageLoader.loadImage(StorePreferences.getInstance().getUserProfilePhoto(), options, new ImageLoadingListener()
                {
                    @Override
                    public void onLoadingStarted(String imageUri, View view)
                    {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason)
                    {
                        if (isAdded())
                        {
                            cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.sample_bg));
                        }
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap bitmap)
                    {
                        if (isAdded())
                        {
                            cimg_user.setImageBitmap(bitmap);
                            img_c_loading.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view)
                    {

                    }
                });
//
//                Picasso.with(getActivity())
//                        .load(StorePreferences.getInstance().getUserProfilePhoto())
//                        .error(R.drawable.sample_bg)
//                        .placeholder(R.drawable.sample_bg)
//                        .into(cimg_user);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        cimg_user = null;
        txt_c_name = null;
        mContext = null;
    }

    public void updateData()
    {
        if (txt_c_name != null && cimg_user != null && mContext != null)
        {
            txt_c_name.setText(StorePreferences.getInstance().get_Name());
            try
            {
                if (!StorePreferences.getInstance().getUserProfilePhoto().equals(""))
                {
                    imageLoader.loadImage(StorePreferences.getInstance().getUserProfilePhoto(), options, new ImageLoadingListener()
                    {
                        @Override
                        public void onLoadingStarted(String imageUri, View view)
                        {
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason)
                        {
                            if (isAdded())
                            {
                                cimg_user.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.sample_bg));
                            }
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap bitmap)
                        {
                            if (isAdded())
                            {
                                cimg_user.setImageBitmap(bitmap);
                            }
                            img_c_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view)
                        {

                        }
                    });

//                    Picasso.with(mContext)
//                            .load(StorePreferences.getInstance().getUserProfilePhoto())
//                            .error(R.drawable.sample_bg)
//                            .placeholder(R.drawable.sample_bg)
//                            .into(cimg_user);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        updateData();
    }

    public void updateUserImageLogo()
    {
        try
        {
            if (!StorePreferences.getInstance().getUserProfilePhoto().equals(""))
            {
                imageLoader.loadImage(StorePreferences.getInstance().getUserProfilePhoto(), options, new ImageLoadingListener()
                {
                    @Override
                    public void onLoadingStarted(String imageUri, View view)
                    {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason)
                    {
                        if (isAdded())
                        {
                            cimg_user.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.sample_bg));
                        }
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap bitmap)
                    {
                        if (isAdded())
                        {
                            cimg_user.setImageBitmap(bitmap);
                        }
                        img_c_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view)
                    {

                    }
                });
//                Picasso.with(getActivity())
//                        .load(StorePreferences.getInstance().getUserProfilePhoto())
//                        .error(R.drawable.sample_bg)
//                        .placeholder(R.drawable.sample_bg)
//                        .into(cimg_user);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void setDrawerListener(FragmentDrawerListener listener)
    {
        this.drawerListener = listener;
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout)
    {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        {
            @Override
            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView)
            {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset)
            {
                super.onDrawerSlide(drawerView, slideOffset);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable()
        {
            @Override
            public void run()
            {
                mDrawerToggle.syncState();
            }
        });

    }

    public static final int PANEL_DASHBOARD = 0, PANEL_MY_PROFILE = 1, PANEL_MY_FAV = 2, PANEL_SEARCH = 3, PANEL_SETTING = 4, PANEL_CONTACT = 5, PANEL_ANUNCEMENT = 6, PANEL_PRIVACY = 7, PANEL_LOGOUT = 8;

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.rl_c_home:
                drawerListener.onDrawerItemSelected(null, PANEL_DASHBOARD);
                mDrawerLayout.closeDrawer(containerView);
                break;
            case R.id.ll_my_profile:
                drawerListener.onDrawerItemSelected(null, PANEL_MY_PROFILE);
                mDrawerLayout.closeDrawer(containerView);
                break;
            case R.id.rl_c_my_profile:
                drawerListener.onDrawerItemSelected(null, PANEL_MY_PROFILE);
                mDrawerLayout.closeDrawer(containerView);
                break;
            case R.id.rl_c_my_fav:
                drawerListener.onDrawerItemSelected(null, PANEL_MY_FAV);
                mDrawerLayout.closeDrawer(containerView);
                break;
            case R.id.rl_c_search:
                drawerListener.onDrawerItemSelected(null, PANEL_SEARCH);
                mDrawerLayout.closeDrawer(containerView);
                break;
            case R.id.rl_c_setting:
                drawerListener.onDrawerItemSelected(null, PANEL_SETTING);
                mDrawerLayout.closeDrawer(containerView);
                break;
            case R.id.rl_c_contact_us:
                drawerListener.onDrawerItemSelected(null, PANEL_CONTACT);
                mDrawerLayout.closeDrawer(containerView);
                break;
            case R.id.rl_c_announcements:
                drawerListener.onDrawerItemSelected(null, PANEL_ANUNCEMENT);
                mDrawerLayout.closeDrawer(containerView);
                break;
            case R.id.rl_c_privacy_policy:
                drawerListener.onDrawerItemSelected(null, PANEL_PRIVACY);
                mDrawerLayout.closeDrawer(containerView);
                break;
            case R.id.rl_c_logout:
                drawerListener.onDrawerItemSelected(null, PANEL_LOGOUT);
                mDrawerLayout.closeDrawer(containerView);
                break;
        }
    }

    public interface FragmentDrawerListener
    {
        void onDrawerItemSelected(View view, int position);
    }
}