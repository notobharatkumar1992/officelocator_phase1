package com.officelocator.fragments;

import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.officelocator.Constants.Tags;
import com.officelocator.R;
import com.officelocator.model.AnnouncementListData;


/**
 * Created by Bharat on 07/19/2016.
 */
public class BannerHomeFragment extends Fragment implements View.OnClickListener {

    private ImageView img_large, img_loading;
    private AnnouncementListData.list sliderModel;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        }
        return inflater.inflate(R.layout.home_banner_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sliderModel = getArguments().getParcelable(Tags.slider_id);
        initView(view);
        setValues();
    }

    private void setValues() {
        img_loading.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
        frameAnimation.setCallback(img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();

        imageLoader.loadImage(sliderModel.logo, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                if (isAdded() && img_large != null) {
                    img_large.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.sample_bg));
                    img_loading.setVisibility(View.GONE);
                }
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                img_large.setImageBitmap(bitmap);
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                if (isAdded() && img_large != null) {
                    img_large.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.sample_bg));
                    img_loading.setVisibility(View.GONE);
                }
            }
        });
    }

    private void initView(View view) {
        img_large = (ImageView) view.findViewById(R.id.img_large);
        img_large.setOnClickListener(this);
        img_loading = (ImageView) view.findViewById(R.id.img_loading);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_large:
                break;
        }
    }
}