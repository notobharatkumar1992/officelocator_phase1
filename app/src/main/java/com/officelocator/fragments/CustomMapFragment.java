package com.officelocator.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.officelocator.AppDelegate;
import com.officelocator.Constants.Tags;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.activity.BranchDetailsActivity;
import com.officelocator.activity.HomeActivity;
import com.officelocator.model.BranchList;
import com.officelocator.model.Person;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.MultiDrawable;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Dev51 on 3/17/2015.
 */
public class CustomMapFragment extends com.google.android.gms.maps.SupportMapFragment implements ClusterManager.OnClusterClickListener<Person>, ClusterManager.OnClusterInfoWindowClickListener<Person>, ClusterManager.OnClusterItemClickListener<Person>, ClusterManager.OnClusterItemInfoWindowClickListener<Person> {

    private String latitude;
    private String longitude;
    private String categoryId;
    GoogleMap googleMap;
    private Marker customMarker;
    private ProgressDialog mProgressDialog;
    private ClusterManager<Person> mClusterManager;
    private Random mRandom = new Random(1984);
    LocationManager locationManager;
    Location location; // location

    //ArrayList declaration
    ArrayList<BranchList.list> BranchArrayList;

    private static final int REQUEST_CODE_LOCATION = 2;
    private Drawable userImg;

    private int int_color = 0;
    private String category_color;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Initialization
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        categoryId = getArguments().getString("categoryId");
        category_color = getArguments().getString("category_color");
        int_color = getArguments().getInt("color");
        BranchArrayList = getArguments().getParcelableArrayList(Tags.value);
        Log.i("categoryId", categoryId + "==");

        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                CustomMapFragment.this.googleMap = googleMap;
                AppDelegate.LogT("Google Map ==" + CustomMapFragment.this.googleMap);
                initView();
            }
        });
    }

    public void initView() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Request missing location permission.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_LOCATION);
        } else {
            // get current location code
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                buildAlertMessageGps();
            } else {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    return;
                }
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLongitude());
                    StorePreferences.getInstance().setLatitude(latitude);
                    StorePreferences.getInstance().setLongitude(longitude);
                } else {
                    latitude = StorePreferences.getInstance().getLatitude();
                    longitude = StorePreferences.getInstance().getLongitude();
                }
                getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        CustomMapFragment.this.googleMap = googleMap;
                        AppDelegate.LogT("Google Map ==" + CustomMapFragment.this.googleMap);
                        checkPermission();
                    }
                });
            }
        }
    }


    private void buildAlertMessageGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getResources().getString(R.string.gps_disable_alert_message))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.gps_disable_alert_yes_button_title), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.gps_disable_alert_no_button_title), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void getBranchByCategoryAPiCall() {
        mProgressDialog.show();//"26.849088", "75.804443"
        String radius = "";
        if (AppDelegate.isValidString(StorePreferences.getInstance().getRadius())) {
            radius = StorePreferences.getInstance().getRadius();
        } else {
            radius = "9";
        }


        SingletonRestClient.get().getBranchByCategory(categoryId, latitude, longitude, radius, StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response,
                                Response response2) {

                mProgressDialog.dismiss();
                if (!isAdded()) {
                    return;
                }
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {
                        JSONArray list = new JSONArray(obj_json.getString("list"));

                        if (list.length() > 0) {
                            BranchArrayList = new ArrayList<BranchList.list>();
                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            BranchList mBranchList = gson.fromJson(String.valueOf(
                                    new String(((TypedByteArray) response.getBody()).getBytes())), BranchList.class);

                            BranchArrayList = mBranchList.list;

//                            int count = 0;
                            for (int i = 0; i < BranchArrayList.size(); i++) {
                                BranchArrayList.get(i).value = int_color;
//                                if (count == AppDelegate.TILES_COLOR_COUNT) {
//                                    count = 0;
//                                } else {
//                                    count++;
//                                }
                            }

                            setValuesOnMap(BranchArrayList);
                        }
                    } else {
                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    AppDelegate.LogE(e);
                    AppDelegate.showToast(getActivity(), "Bad server response.");
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });
    }

    private void setValuesOnMap(final ArrayList<BranchList.list> BranchArrayList) {
        Log.i("BranchArrayList", BranchArrayList + "==");
        mClusterManager = new ClusterManager<Person>(getActivity(), googleMap);
        mClusterManager.setRenderer(new PersonRenderer(googleMap));
        googleMap.setOnCameraIdleListener(mClusterManager);
        googleMap.setOnMarkerClickListener(mClusterManager);
        googleMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(CustomMapFragment.this);
        mClusterManager.setOnClusterInfoWindowClickListener(CustomMapFragment.this);
        mClusterManager.setOnClusterItemClickListener(CustomMapFragment.this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(CustomMapFragment.this);

        AppDelegate.LogT("start demo");
        LatLng myLocation = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
        try {
            myLocation = new LatLng(Double.valueOf(StorePreferences.getInstance().getLatitude()), Double.valueOf(StorePreferences.getInstance().getLongitude()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 10));


        for (int i = 0; i < BranchArrayList.size(); i++) {
            try {
                double lat = Double.parseDouble(BranchArrayList.get(i).latitude);
                double lon = Double.parseDouble(BranchArrayList.get(i).longitude);
                LatLng latlong = new LatLng(lat, lon);
                imageLoader.loadImage(BranchArrayList.get(i).logo, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        if (isAdded())
                            userImg = getActivity().getResources().getDrawable(R.drawable.sample_bg);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        if (isAdded())
                            userImg = getActivity().getResources().getDrawable(R.drawable.sample_bg);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                        if (isAdded())
                            userImg = new BitmapDrawable(getResources(), bitmap);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {

                    }
                });
//                Picasso.with(getActivity())
//                        .load(BranchArrayList.get(i).logo)
//                        .into(new Target() {
//                            @Override
//                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                                userImg = new BitmapDrawable(getResources(), bitmap);
//                            }
//
//                            @Override
//                            public void onBitmapFailed(Drawable errorDrawable) {
//                                userImg = getActivity().getResources().getDrawable(R.drawable.sample_bg);
//                            }
//
//                            @Override
//                            public void onPrepareLoad(Drawable placeHolderDrawable) {
//                                userImg = getActivity().getResources().getDrawable(R.drawable.sample_bg);
//                            }
//                        });
                mClusterManager.addItem(new Person(latlong, BranchArrayList.get(i).id, BranchArrayList.get(i).name, userImg, BranchArrayList.get(i).value));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mClusterManager.cluster();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_LOCATION) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    //code here
                    Log.i("categoryId", categoryId + "==");
                    // get current location code
                    locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                    if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        buildAlertMessageGps();
                    } else {
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = String.valueOf(location.getLatitude());
                            longitude = String.valueOf(location.getLongitude());
                            StorePreferences.getInstance().setLatitude(latitude);
                            StorePreferences.getInstance().setLongitude(longitude);
                        } else {
                            latitude = StorePreferences.getInstance().getLatitude();
                            longitude = StorePreferences.getInstance().getLongitude();
                        }
                        getMapAsync(new OnMapReadyCallback() {
                            @Override
                            public void onMapReady(GoogleMap googleMap) {
                                CustomMapFragment.this.googleMap = googleMap;
                                AppDelegate.LogT("Google Map ==" + CustomMapFragment.this.googleMap);
                                checkPermission();
                            }
                        });
                        //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, obj_map_location_listner);

                    }
                }
            } else {
                // Permission was denied. Display an error message.
            }
        }
    }

    private void checkPermission() {
        latitude = "19.36889400";
        longitude = "-99.1803240000000000";
        StorePreferences.getInstance().setLatitude(latitude);
        StorePreferences.getInstance().setLongitude(longitude);
        try {
            googleMap.getUiSettings().setMapToolbarEnabled(false);
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            if (AppDelegate.isValidString(latitude) && AppDelegate.isValidString(longitude)) {
                LatLng myLocation = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
                googleMap.addMarker(new MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.userpin)).title(getString(R.string.map_user_pin_title)));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
            }
            if (BranchArrayList == null || BranchArrayList.size() == 0) {
                if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                    getBranchByCategoryAPiCall();
                } else {
                    ConnectionDetector.getInstance().show_alert(getActivity());
                }
            } else {
                setValuesOnMap(BranchArrayList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*=============================show dialog map pin=================================*/
    @Override
    public boolean onClusterClick(Cluster<Person> cluster) {
        AppDelegate.LogT("onClusterClick => ");
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : cluster.getItems()) {
            builder.include(item.getPosition());
        }
        // Get the LatLngBounds
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Person> cluster) {
        AppDelegate.LogT("onClusterInfoWindowClick => ");
        if (item == null)
            return;
        String id = item.id;
        for (int i = 0; i < BranchArrayList.size(); i++) {
            if (BranchArrayList.get(i).id.equals(id)) {
                Bundle bundle = new Bundle();
                bundle.putString("Id", BranchArrayList.get(i).id);
                bundle.putString("BranchId", BranchArrayList.get(i).category_id);
                bundle.putString("category_color", BranchArrayList.get(i).category_color);
                bundle.putInt("color", BranchArrayList.get(i).value);

                Intent intent = new Intent(getActivity(), BranchDetailsActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false,
                        new Pair<>(((HomeActivity) getActivity()).findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
                intent.putExtras(bundle);
                startActivity(intent, transitionActivityOptions.toBundle());
            }
        }
    }

    public Person item;

    @Override
    public boolean onClusterItemClick(Person item) {
        AppDelegate.LogT("onClusterItemClick => ");
        this.item = item;
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Person item) {
        AppDelegate.LogT("onClusterItemInfoWindowClick => ");
        if (item != null) {
            String id = item.id;
            for (int i = 0; i < BranchArrayList.size(); i++) {
                if (BranchArrayList.get(i).id.equals(id)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("Id", BranchArrayList.get(i).id);
                    bundle.putString("BranchId", BranchArrayList.get(i).category_id);
                    bundle.putString("category_color", BranchArrayList.get(i).category_color);
                    bundle.putInt("color", BranchArrayList.get(i).value);
                    AppDelegate.LogT("CustomMapFragment Send to detail = " + BranchArrayList.get(i).value);

                    Intent intent = new Intent(getActivity(), BranchDetailsActivity.class);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false,
                            new Pair<>(((HomeActivity) getActivity()).findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
                    intent.putExtras(bundle);
                    startActivity(intent, transitionActivityOptions.toBundle());
                }
            }
        }
    }

    public class PersonRenderer extends DefaultClusterRenderer<Person> {
        private final IconGenerator mIconGenerator = new IconGenerator(getActivity());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getActivity());
        private final ImageView mClusterImageView;
        private final ImageView img_marker;
        private final int mDimension;

        public PersonRenderer(GoogleMap googleMap) {
            super(getActivity(), googleMap, mClusterManager);

            View multiProfile = getActivity().getLayoutInflater().inflate(R.layout.multi_profile, null);
            View multiProfileone = getActivity().getLayoutInflater().inflate(R.layout.multi_profile, null);
            switch (AppDelegate.getRandomNumber()) {
                case 0:
                    multiProfile = getActivity().getLayoutInflater().inflate(R.layout.multi_profile, null);
                    multiProfileone = getActivity().getLayoutInflater().inflate(R.layout.multi_profile, null);
                    break;
                case 1:
                    multiProfile = getActivity().getLayoutInflater().inflate(R.layout.multi_profile_1, null);
                    multiProfileone = getActivity().getLayoutInflater().inflate(R.layout.multi_profile_1, null);
                    break;
                case 2:
                    multiProfile = getActivity().getLayoutInflater().inflate(R.layout.multi_profile_2, null);
                    multiProfileone = getActivity().getLayoutInflater().inflate(R.layout.multi_profile_2, null);
                    break;
                case 3:
                    multiProfile = getActivity().getLayoutInflater().inflate(R.layout.multi_profile_3, null);
                    multiProfileone = getActivity().getLayoutInflater().inflate(R.layout.multi_profile_3, null);
                    break;
                case 4:
                    multiProfile = getActivity().getLayoutInflater().inflate(R.layout.multi_profile_4, null);
                    multiProfileone = getActivity().getLayoutInflater().inflate(R.layout.multi_profile_4, null);
                    break;

            }
            mClusterIconGenerator.setContentView(multiProfileone);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);
            img_marker = (ImageView) multiProfile.findViewById(R.id.img_marker);

            // mImageView = new ImageView(getActivity());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            //mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            // mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(multiProfile);
        }

        @Override
        protected void onBeforeClusterItemRendered(Person person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.
            mClusterImageView.setImageDrawable(person.profilePhoto);
            switch (AppDelegate.getRandomNumber()) {
                case 0:
                    img_marker.setImageResource(R.drawable.marker_1);
                    break;
                case 1:
                    img_marker.setImageResource(R.drawable.marker_2);
                    break;
                case 2:
                    img_marker.setImageResource(R.drawable.marker_3);
                    break;
                case 3:
                    img_marker.setImageResource(R.drawable.marker_4);
                    break;
                case 4:
                    img_marker.setImageResource(R.drawable.marker_5);
                    break;
                default:
                    img_marker.setImageResource(R.drawable.marker_1);
                    break;
            }
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(person.name);
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Person> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Person p : cluster.getItems()) {
                // Draw 4 at most.
//                if (profilePhotos.size() == 4) break;
                /*Drawable drawable =p.profilePhoto;
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);*/
                mClusterImageView.setImageDrawable(p.profilePhoto);
                switch (AppDelegate.getRandomNumber()) {
                    case 0:
                        img_marker.setImageResource(R.drawable.marker_1);
                        break;
                    case 1:
                        img_marker.setImageResource(R.drawable.marker_2);
                        break;
                    case 2:
                        img_marker.setImageResource(R.drawable.marker_3);
                        break;
                    case 3:
                        img_marker.setImageResource(R.drawable.marker_4);
                        break;
                    case 4:
                        img_marker.setImageResource(R.drawable.marker_5);
                        break;
                    default:
                        img_marker.setImageResource(R.drawable.marker_1);
                        break;
                }
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }
}
