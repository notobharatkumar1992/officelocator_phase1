package com.officelocator.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.activity.BranchDetailLocationActivity;
import com.officelocator.util.CircleImageView;
import com.officelocator.util.StorePreferences;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Dev51 on 3/17/2015.
 */
public class BranchDetailLocationDirectionMapFragment extends com.google.android.gms.maps.SupportMapFragment
{
    private int int_color = 0;
    private String currentLatitude;
    private String currentLongitude;
    private String destinationLatitude;
    private String destinationLongitude;
    private String pinLogo;
    GoogleMap googleMap;
    private Marker customMarker;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        currentLatitude = StorePreferences.getInstance().getLatitude();
        currentLongitude = StorePreferences.getInstance().getLongitude();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        int_color = getArguments().getInt("color");
        destinationLatitude = getArguments().getString("destinationLatitude");
        destinationLongitude = getArguments().getString("destinationLongitude");
        pinLogo = getArguments().getString("pinLogo");
        AppDelegate.LogT("received at BranchDetailOpenMapFullFragment color => " + int_color);

        Log.e("pinLogo", pinLogo + "=");

        getMapAsync(new OnMapReadyCallback()
        {
            @Override
            public void onMapReady(GoogleMap googleMap)
            {
                BranchDetailLocationDirectionMapFragment.this.googleMap = googleMap;
                new DownloadImage(pinLogo).execute();
            }
        });
    }

    public Bitmap remote_picture = null;

    public class DownloadImage extends AsyncTask<Void, Void, Void>
    {

        public String str_url = null;

        public DownloadImage(String pinLogo)
        {
            str_url = pinLogo;
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            AppDelegate.setStictModePermission();
            try
            {
                remote_picture = BitmapFactory.decodeStream((InputStream) new URL(str_url).getContent());
                AppDelegate.LogT("Detail page = downloaded image => ");
            }
            catch (IOException e)
            {
                AppDelegate.LogE(e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            initView();
        }
    }

    private void initView()
    {
        try
        {
            LatLng myLocation = new LatLng(Double.valueOf(currentLatitude), Double.valueOf(currentLongitude));
            if (getActivity() != null)
            {
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.addMarker(new MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.userpin)).title(getActivity().getResources().getString(R.string.map_user_pin_title)));
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
                {
                    @Override
                    public boolean onMarkerClick(Marker marker)
                    {
                        /*Uri gmmIntentUri = Uri.parse("geo:" + currentLatitude + "," + currentLongitude);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        //mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);*/

                        showDialog();
                        return false;
                    }
                });

                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

                LatLng destinationLocation = new LatLng(Double.valueOf(destinationLatitude), Double.valueOf(destinationLongitude));
                View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout_demo, null);
                final CircleImageView categoryImageView = (CircleImageView) marker.findViewById(R.id.categoryRoundedImageView);
                ImageView img_marker = (ImageView) marker.findViewById(R.id.img_marker);
                switch (int_color)
                {
                    case 0:
                        img_marker.setImageResource(R.drawable.marker_1);
                        break;
                    case 1:
                        img_marker.setImageResource(R.drawable.marker_2);
                        break;
                    case 2:
                        img_marker.setImageResource(R.drawable.marker_3);
                        break;
                    case 3:
                        img_marker.setImageResource(R.drawable.marker_4);
                        break;
                    case 4:
                        img_marker.setImageResource(R.drawable.marker_5);
                        break;
                    default:
                        img_marker.setImageResource(R.drawable.marker_1);
                        break;
                }

                AppDelegate.LogT("Map detail page = " + remote_picture);
                if (remote_picture != null)
                {
                    categoryImageView.setImageBitmap(remote_picture);
                }
                else
                {
                    imageLoader.loadImage(pinLogo, options, new ImageLoadingListener()
                    {
                        @Override
                        public void onLoadingStarted(String imageUri, View view)
                        {
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason)
                        {
                            if (isAdded())
                            {
                                categoryImageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.sample_bg));
                            }
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap bitmap)
                        {
                            categoryImageView.setImageBitmap(bitmap);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view)
                        {

                        }
                    });
                }
//                    Picasso.with(getActivity())
//                            .load(pinLogo)
//                            .error(R.drawable.sample_bg)
//                            .placeholder(R.drawable.sample_bg)
//                            .into(categoryImageView);

                // Bitmap bmp = getBitmapFromURL(BranchArrayList.get(i).logo);
                customMarker = googleMap.addMarker(new MarkerOptions()
                        .position(destinationLocation)
                        .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker))));

                // m.showInfoWindow();

                PolylineOptions polylineOptions = new PolylineOptions();

                ArrayList<LatLng> points = new ArrayList<LatLng>();

                // Setting the color of the polyline
                polylineOptions.color(Color.RED);

                // Setting the width of the polyline
                polylineOptions.width(5);

                // Adding the taped point to the ArrayList
                points.add(myLocation);
                points.add(destinationLocation);

                // Setting points of polyline
                polylineOptions.addAll(points);

                // Adding the polyline to the map
                googleMap.addPolyline(polylineOptions);
            }

//            final Handler handler = new Handler();
//            final Runnable r = new Runnable() {
//                public void run() {
//
//                    if (getActivity() != null) {
//                        googleMap.clear();
//                        googleMap.getUiSettings().setZoomControlsEnabled(true);
//                        LatLng myLocation = new LatLng(Double.valueOf(currentLatitude), Double.valueOf(currentLongitude));
//                        googleMap.addMarker(new MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.userpin)).title(getActivity().getResources().getString(R.string.map_user_pin_title)));
//
//                        LatLng destinationLocation = new LatLng(Double.valueOf(destinationLatitude), Double.valueOf(destinationLongitude));
//
//                        View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout_demo, null);
//                        CircleImageView categoryImageView = (CircleImageView) marker.findViewById(R.id.categoryRoundedImageView);
//                        Picasso.with(getActivity())
//                                .load(pinLogo)
//                                .error(R.drawable.sample_bg)
//                                .placeholder(R.drawable.sample_bg)
//                                .into(categoryImageView);
//
//                        // Bitmap bmp = getBitmapFromURL(BranchArrayList.get(i).logo);
//                        customMarker = googleMap.addMarker(new MarkerOptions()
//                                .position(destinationLocation)
//                                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker))));
//
//                        // m.showInfoWindow();
//
//                        PolylineOptions polylineOptions = new PolylineOptions();
//
//                        ArrayList<LatLng> points = new ArrayList<LatLng>();
//
//                        // Setting the color of the polyline
//                        polylineOptions.color(Color.RED);
//
//                        // Setting the width of the polyline
//                        polylineOptions.width(5);
//
//                        // Adding the taped point to the ArrayList
//                        points.add(myLocation);
//                        points.add(destinationLocation);
//
//                        // Setting points of polyline
//                        polylineOptions.addAll(points);
//
//                        // Adding the polyline to the map
//                        googleMap.addPolyline(polylineOptions);
//                        //handler.postDelayed(this, 10000);
//                    }
//                }
//            };
//
//            handler.postDelayed(r, 2500);

            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
            {

                @Override
                public void onMapClick(LatLng point)
                {
                    Intent intent = new Intent(getActivity(), BranchDetailLocationActivity.class);
                    Log.d("Map", "Map clicked");
                    Bundle bundle = new Bundle();
                    bundle.putString("destinationLatitude", destinationLatitude);
                    bundle.putString("destinationLongitude", destinationLongitude);
                    bundle.putString("pinLogo", pinLogo);
                    bundle.putInt("color", int_color);
                    intent.putExtras(bundle);
                    intent.putExtras(bundle);
//                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false,
//                            new Pair<>(((BranchDetailsActivity) getActivity()).findViewById(R.id.img_topbar), getString(R.string.top_bar)));
//                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
                    startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view)
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    void showDialog()
    {
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(R.layout.bottom_map_dialog);
        dialog.show();

        dialog.findViewById(R.id.googleMap).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + currentLatitude + "," + currentLongitude);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        dialog.findViewById(R.id.wazeMap).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                String url = "https://waze.com/ul?ll=" + currentLatitude + "," + currentLongitude;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.setPackage("com.waze");
                startActivity(intent);
            }
        });
    }
}