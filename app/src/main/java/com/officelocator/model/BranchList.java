package com.officelocator.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.util.ArrayList;

public class BranchList implements ClusterItem, Parcelable {
    public ArrayList<list> list;
    private LatLng mPosition;

    public BranchList(double lat, double lang) {
        mPosition = new LatLng(lat, lang);
    }

    protected BranchList(Parcel in) {
        mPosition = in.readParcelable(LatLng.class.getClassLoader());
    }

    public static final Creator<BranchList> CREATOR = new Creator<BranchList>() {
        @Override
        public BranchList createFromParcel(Parcel in) {
            return new BranchList(in);
        }

        @Override
        public BranchList[] newArray(int size) {
            return new BranchList[size];
        }
    };

    public void setmPosition(double lat, double lang) {
        mPosition = new LatLng(lat, lang);

    }

    @Override
    public LatLng getPosition() {

        return mPosition;
    }

    @Override
    public String toString() {
        return "BranchList{" +
                "list=" + list +
                ", mPosition=" + mPosition +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mPosition, flags);
    }

    public static class list implements Parcelable {
        public String id;
        public String category_color;
        public int value;
        public String category_id;
        public String category_name;
        public String name;
        public String type;
        public String created;
        public String modified;
        public String status;
        public String logo;
        public String country;
        public String state;
        public String city;
        public String zip;
        public String address;
        public String address2;
        public String landmark;
        public String latitude;
        public String longitude;
        public String start_date;
        public String end_date;

        protected list(Parcel in) {
            id = in.readString();
            category_color = in.readString();
            value = in.readInt();
            category_id = in.readString();
            category_name = in.readString();
            name = in.readString();
            type = in.readString();
            created = in.readString();
            modified = in.readString();
            status = in.readString();
            logo = in.readString();
            country = in.readString();
            state = in.readString();
            city = in.readString();
            zip = in.readString();
            address = in.readString();
            address2 = in.readString();
            landmark = in.readString();
            latitude = in.readString();
            longitude = in.readString();
            start_date = in.readString();
            end_date = in.readString();
        }

        public static final Creator<list> CREATOR = new Creator<list>() {
            @Override
            public list createFromParcel(Parcel in) {
                return new list(in);
            }

            @Override
            public list[] newArray(int size) {
                return new list[size];
            }
        };

        @Override
        public String toString() {
            return "list{" +
                    "id='" + id + '\'' +
                    "category_color='" + category_color + '\'' +
                    "value='" + value + '\'' +
                    ", category_id='" + category_id + '\'' +
                    ", category_name='" + category_name + '\'' +
                    ", name='" + name + '\'' +
                    ", type='" + type + '\'' +
                    ", created='" + created + '\'' +
                    ", modified='" + modified + '\'' +
                    ", status='" + status + '\'' +
                    ", logo='" + logo + '\'' +
                    ", country='" + country + '\'' +
                    ", state='" + state + '\'' +
                    ", city='" + city + '\'' +
                    ", zip='" + zip + '\'' +
                    ", address='" + address + '\'' +
                    ", address2='" + address2 + '\'' +
                    ", landmark='" + landmark + '\'' +
                    ", latitude='" + latitude + '\'' +
                    ", longitude='" + longitude + '\'' +
                    ", start_date='" + start_date + '\'' +
                    ", end_date='" + end_date + '\'' +
                    '}';
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(category_color);
            dest.writeInt(value);
            dest.writeString(category_id);
            dest.writeString(category_name);
            dest.writeString(name);
            dest.writeString(type);
            dest.writeString(created);
            dest.writeString(modified);
            dest.writeString(status);
            dest.writeString(logo);
            dest.writeString(country);
            dest.writeString(state);
            dest.writeString(city);
            dest.writeString(zip);
            dest.writeString(address);
            dest.writeString(address2);
            dest.writeString(landmark);
            dest.writeString(latitude);
            dest.writeString(longitude);
            dest.writeString(start_date);
            dest.writeString(end_date);
        }
    }
}
