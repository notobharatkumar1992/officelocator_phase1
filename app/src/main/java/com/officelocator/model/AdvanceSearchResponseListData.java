package com.officelocator.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.util.ArrayList;

public class AdvanceSearchResponseListData implements ClusterItem, Parcelable {

    public ArrayList<list> list;
    private LatLng mPosition;

    protected AdvanceSearchResponseListData(Parcel in) {
        mPosition = in.readParcelable(LatLng.class.getClassLoader());
    }

    public static final Creator<AdvanceSearchResponseListData> CREATOR = new Creator<AdvanceSearchResponseListData>() {
        @Override
        public AdvanceSearchResponseListData createFromParcel(Parcel in) {
            return new AdvanceSearchResponseListData(in);
        }

        @Override
        public AdvanceSearchResponseListData[] newArray(int size) {
            return new AdvanceSearchResponseListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mPosition, flags);
    }

    @Override
    public LatLng getPosition() {
        return null;
    }

    public static class list implements Parcelable {
        public String id;
        public String category_color;
        public int value;
        public ArrayList<product> product;
        public ArrayList<service> service;
        public String category_id;
        public String category_name;
        public String name;
        public String type;
        public String created;
        public String modified;
        public String status;
        public String logo;
        public String country;
        public String state;
        public String city;
        public String zip;
        public String address;
        public String address2;
        public String landmark;
        public String latitude;
        public String longitude;
        public String start_date;
        public String end_date;

        protected list(Parcel in) {
            id = in.readString();
            category_color = in.readString();
            value = in.readInt();
//            product = in.createTypedArrayList(list.product.CREATOR);
//            service = in.createTypedArrayList(AdvanceSearchResponseListData.list.service.CREATOR);
            category_id = in.readString();
            category_name = in.readString();
            name = in.readString();
            type = in.readString();
            created = in.readString();
            modified = in.readString();
            status = in.readString();
            logo = in.readString();
            country = in.readString();
            state = in.readString();
            city = in.readString();
            zip = in.readString();
            address = in.readString();
            address2 = in.readString();
            landmark = in.readString();
            latitude = in.readString();
            longitude = in.readString();
            start_date = in.readString();
            end_date = in.readString();
        }

        public static final Creator<list> CREATOR = new Creator<list>() {
            @Override
            public list createFromParcel(Parcel in) {
                return new list(in);
            }

            @Override
            public list[] newArray(int size) {
                return new list[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(category_color);
            dest.writeInt(value);
            dest.writeTypedList(product);
            dest.writeTypedList(service);
            dest.writeString(category_id);
            dest.writeString(category_name);
            dest.writeString(name);
            dest.writeString(type);
            dest.writeString(created);
            dest.writeString(modified);
            dest.writeString(status);
            dest.writeString(logo);
            dest.writeString(country);
            dest.writeString(state);
            dest.writeString(city);
            dest.writeString(zip);
            dest.writeString(address);
            dest.writeString(address2);
            dest.writeString(landmark);
            dest.writeString(latitude);
            dest.writeString(longitude);
            dest.writeString(start_date);
            dest.writeString(end_date);
        }


        public static class product implements Parcelable {
            public String id;
            public String name;
            public String name_spanish;
            public String status;
            public String created;
            public String modified;

            protected product(Parcel in) {
                id = in.readString();
                name = in.readString();
                name_spanish = in.readString();
                status = in.readString();
                created = in.readString();
                modified = in.readString();
            }

            public static final Creator<product> CREATOR = new Creator<product>() {
                @Override
                public product createFromParcel(Parcel in) {
                    return new product(in);
                }

                @Override
                public product[] newArray(int size) {
                    return new product[size];
                }
            };

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(id);
                dest.writeString(name);
                dest.writeString(name_spanish);
                dest.writeString(status);
                dest.writeString(created);
                dest.writeString(modified);
            }
        }

        public static class service implements Parcelable {
            public String id;
            public String name;
            public String name_spanish;
            public String status;
            public String created;
            public String modified;

            protected service(Parcel in) {
                id = in.readString();
                name = in.readString();
                name_spanish = in.readString();
                status = in.readString();
                created = in.readString();
                modified = in.readString();
            }

            public static final Creator<service> CREATOR = new Creator<service>() {
                @Override
                public service createFromParcel(Parcel in) {
                    return new service(in);
                }

                @Override
                public service[] newArray(int size) {
                    return new service[size];
                }
            };

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(id);
                dest.writeString(name);
                dest.writeString(name_spanish);
                dest.writeString(status);
                dest.writeString(created);
                dest.writeString(modified);
            }
        }
    }
}
